import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
/* import footer from './footer-02.json'; */
import footer from '../../../assets/json/footer-02.json';
let BottomComponent = class BottomComponent {
    constructor() {
        this.footer = footer['footer-02'];
    }
    ngOnInit() {
    }
};
BottomComponent = tslib_1.__decorate([
    Component({
        selector: 'app-bottom',
        templateUrl: './bottom.component.html',
        styleUrls: ['./bottom.component.css']
    })
], BottomComponent);
export { BottomComponent };
//# sourceMappingURL=bottom.component.js.map