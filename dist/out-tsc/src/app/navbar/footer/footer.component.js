import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
//import footer                 from './footer-01.json';
import footer from '../../../assets/json/footer-01.json';
import MenuRepos from '../../../assets/json/menu-repositorio.json';
let FooterComponent = class FooterComponent {
    constructor() {
        //json:       any = json;
        this.footer = footer['footer-01'];
        this.menuRepos = MenuRepos['menu-repositorio-uc'];
    }
    ngOnInit() {
        $("#msjFooter").hide();
    }
    alertasFooter(estilo, icono, texto) {
        $("#msjFooter").fadeIn(1000);
        setInterval(() => {
            $("#msjFooter").fadeOut(1500);
        }, 2000);
        this.msjf = texto;
        this.estilof = estilo;
        this.iconof = icono;
    }
};
FooterComponent = tslib_1.__decorate([
    Component({
        selector: 'app-footer',
        templateUrl: './footer.component.html',
        styleUrls: ['./footer.component.css']
    })
], FooterComponent);
export { FooterComponent };
//# sourceMappingURL=footer.component.js.map