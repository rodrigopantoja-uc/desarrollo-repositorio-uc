import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let CardBibliometricsComponent = class CardBibliometricsComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib_1.__decorate([
    Input()
], CardBibliometricsComponent.prototype, "cardData", void 0);
CardBibliometricsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-card-bibliometrics',
        templateUrl: './card-bibliometrics.component.html',
        styleUrls: ['./card-bibliometrics.component.css']
    })
], CardBibliometricsComponent);
export { CardBibliometricsComponent };
//# sourceMappingURL=card-bibliometrics.component.js.map