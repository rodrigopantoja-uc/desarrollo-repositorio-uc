import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let CardPublicationsComponent = class CardPublicationsComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib_1.__decorate([
    Input()
], CardPublicationsComponent.prototype, "data", void 0);
tslib_1.__decorate([
    Input()
], CardPublicationsComponent.prototype, "heightSame", void 0);
tslib_1.__decorate([
    Input()
], CardPublicationsComponent.prototype, "urlImg", void 0);
CardPublicationsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-card-publications',
        templateUrl: './card-publications.component.html',
        styleUrls: ['./card-publications.component.css']
    })
], CardPublicationsComponent);
export { CardPublicationsComponent };
//# sourceMappingURL=card-publications.component.js.map