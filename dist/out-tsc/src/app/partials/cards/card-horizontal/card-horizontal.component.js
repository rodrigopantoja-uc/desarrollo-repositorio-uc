import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let CardHorizontalComponent = class CardHorizontalComponent {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    ngOnInit() {
    }
    getBackgroundImage(image) {
        return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
    }
};
tslib_1.__decorate([
    Input()
], CardHorizontalComponent.prototype, "data", void 0);
tslib_1.__decorate([
    Input()
], CardHorizontalComponent.prototype, "dataOrientation", void 0);
tslib_1.__decorate([
    Input()
], CardHorizontalComponent.prototype, "dataDivider", void 0);
tslib_1.__decorate([
    Input()
], CardHorizontalComponent.prototype, "excerpt", void 0);
CardHorizontalComponent = tslib_1.__decorate([
    Component({
        selector: 'app-card-horizontal',
        templateUrl: './card-horizontal.component.html',
        styleUrls: ['./card-horizontal.component.css']
    })
], CardHorizontalComponent);
export { CardHorizontalComponent };
//# sourceMappingURL=card-horizontal.component.js.map