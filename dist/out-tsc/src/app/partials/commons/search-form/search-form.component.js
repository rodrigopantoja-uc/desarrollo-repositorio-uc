import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let SearchFormComponent = class SearchFormComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    getSearch(form, $event) {
        localStorage.setItem('search_form', form.form.value.ucsearch);
        var array_Filtros = [
            {
                search_by: 'tipo',
                contains: 'contiene',
                term: this.pagina
            }
        ];
        localStorage.setItem('json_filtros', JSON.stringify(array_Filtros));
        localStorage.setItem('searchAdvanced', 'true');
        localStorage.setItem('page', '1');
        this.router.navigate(['/busqueda']);
    }
    BusquedaAvanzada() {
        localStorage.setItem('search_form', '');
        var array_Filtros = [
            {
                search_by: 'tipo',
                contains: 'contiene',
                term: this.pagina
            }
        ];
        localStorage.setItem('json_filtros', JSON.stringify(array_Filtros));
        localStorage.setItem('searchAdvanced', 'true');
        localStorage.setItem('page', '1');
        this.router.navigate(['/busqueda']);
    }
};
tslib_1.__decorate([
    Input()
], SearchFormComponent.prototype, "data", void 0);
tslib_1.__decorate([
    Input()
], SearchFormComponent.prototype, "pagina", void 0);
SearchFormComponent = tslib_1.__decorate([
    Component({
        selector: 'app-search-form',
        templateUrl: './search-form.component.html',
        styleUrls: ['./search-form.component.css']
    })
], SearchFormComponent);
export { SearchFormComponent };
//# sourceMappingURL=search-form.component.js.map