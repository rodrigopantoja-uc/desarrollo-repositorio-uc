import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './templates/home/home.component';
import { ContactComponent } from './templates/contact/contact.component';
import { CoverSecondLevelComponent } from './templates/cover-second-level/cover-second-level.component';
import { PageContentComponent } from './templates/page-content/page-content.component';
import { SingleRecordComponent } from './templates/single-record/single-record.component';
import { UploadRecordComponent } from './templates/upload-record/upload-record.component';
import { DatosInvestigacionComponent } from './templates/datos-investigacion/datos-investigacion.component';
import { DescargaGaComponent } from './templates/descarga-ga/descarga-ga.component';
import { BusquedaComponent } from './templates/busqueda/busqueda.component';
import { IdentityGuard } from './services/identity.guard';
//import { BibliometricsComponent }     from './templates/bibliometrics/bibliometrics.component';
//import { BlogComponent }              from './templates/blog/blog.component';
//import { BlogListComponent }          from './templates/blog-list/blog-list.component';
//import { CoverContentComponent }      from './templates/cover-content/cover-content.component';
//import { FaqComponent }               from './templates/faq/faq.component';
const routes = [
    { path: '', component: HomeComponent },
    { path: 'bibliometrics', loadChildren: () => import('./templates/bibliometrics/bibliometrics.module').then(m => m.BibliometricsModule) },
    { path: 'blog-list', loadChildren: () => import('./templates/blog-list/blog-list.module').then(m => m.BlogListModule) },
    { path: 'blog/:id', loadChildren: () => import('./templates/blog/blog.module').then(m => m.BlogModule) },
    { path: 'faq', loadChildren: () => import('./templates/faq/faq.module').then(m => m.FaqModule) },
    { path: 'enlaces/:pag', loadChildren: () => import('./templates/cover-content/cover-content.module').then(m => m.CoverContentModule) },
    //{path: 'content/:pag',loadChildren: ()  => import('./templates/page-content/page-content.module').then(m => m.PageContentModule)},
    //{path: 'contact',loadChildren: () => import('./templates/contact/contact.module').then(m => m.ContactModule)},
    { path: 'contact', component: ContactComponent },
    { path: 'busqueda', component: BusquedaComponent },
    { path: 'pagina/:pag', component: CoverSecondLevelComponent },
    { path: 'upload/:pag', component: UploadRecordComponent, canActivate: [IdentityGuard] },
    { path: 'datos-investigacion', component: DatosInvestigacionComponent, canActivate: [IdentityGuard] },
    { path: 'content/:pag', component: PageContentComponent },
    { path: 'handle/:param1/:param2', component: SingleRecordComponent },
    { path: 'descarga-ficha/:titulo', component: DescargaGaComponent },
    //{ path: 'bibliometrics',      component: BibliometricsComponent },
    //{ path: 'blog/:id',           component: BlogComponent },
    //{ path: 'blog-list',          component: BlogListComponent },
    //{ path: 'faq',                component: FaqComponent },
    //{ path: 'enlaces/:pag',       component: CoverContentComponent },
    /* Ruta wildcard, si la ruta no existe, redirige al Home */
    { path: '**', redirectTo: '' }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map