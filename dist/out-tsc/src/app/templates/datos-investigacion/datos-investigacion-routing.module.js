import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatosInvestigacionComponent } from './datos-investigacion.component';
const routes = [
    { path: '', component: DatosInvestigacionComponent }
];
let DatosInvestigacionRoutingModule = class DatosInvestigacionRoutingModule {
};
DatosInvestigacionRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], DatosInvestigacionRoutingModule);
export { DatosInvestigacionRoutingModule };
//# sourceMappingURL=datos-investigacion-routing.module.js.map