import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatosInvestigacionRoutingModule } from './datos-investigacion-routing.module';
import { DatosInvestigacionComponent } from './datos-investigacion.component';
let DatosInvestigacionModule = class DatosInvestigacionModule {
};
DatosInvestigacionModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            DatosInvestigacionComponent
        ],
        imports: [
            CommonModule,
            DatosInvestigacionRoutingModule
        ]
    })
], DatosInvestigacionModule);
export { DatosInvestigacionModule };
//# sourceMappingURL=datos-investigacion.module.js.map