import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import json from '../../../assets/json/datos-investigacion/datos-investigacion.json';
import compartir from '../../../assets/json/upload-record/02-compartir-investigacion.json';
import { global } from '../../services/global';
let DatosInvestigacionComponent = class DatosInvestigacionComponent {
    constructor(uploadService, _queriesService, router, activatedRoute, http) {
        this.uploadService = uploadService;
        this._queriesService = _queriesService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.data = [];
        this.json = json;
        this.compartir = compartir;
        this.activePane = 't1';
        this.tipo_responsable = "Persona";
        this.titulo_cc = "Licencia Creative Commons Atribución 4.0 Internacional";
        this.url_cc = "https://creativecommons.org/licenses/by/4.0/legalcode.es";
        this.img_cc = "../../../assets/img/cc_40.png";
        this.img_licencia = "../../../assets/img/seal.png";
        this.descripcion_licencia = "¡Esta es una Licencia de Cultura Libre!";
        this.array_di = {
            clave: this._queriesService.getPass(),
            user: localStorage.getItem("correo"),
            titulo: null,
            descripcion: null,
            keyword: null,
            licencia_cc: this.titulo_cc,
            cc_compartir: "cc_sicomparte",
            cc_comercial: "cc_sicomercial",
            concedo_licencia: null,
            responsable: {
                persona: [{
                        apellido: null,
                        nombre: null
                    }],
                unidad: [{
                        nombre: null
                    }]
            },
            publicaciones_relacionadas: [{
                    titulo: null,
                    url: null
                }],
            archivos: [{
                    archivo: null,
                    descripcion: null,
                    fecha_embargo: null
                }]
        };
        this.urlfile = global.php + "/autoarchivo/subirArchivo.php";
        this.identity = this._queriesService.getIdentity();
        this.token = this._queriesService.getToken();
        this.password = this._queriesService.getPass();
    }
    ngOnInit() {
        // refresca página
        this._queriesService.getRefresh();
        this.date();
        this.usuario = localStorage.getItem('usuario');
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    }
    ngDoCheck() {
        this.password = this._queriesService.getPass();
    }
    addPersona() {
        this.array_di.responsable.persona.push({
            nombre: '',
            apellido: ''
        });
        console.log(this.array_di);
    }
    removePersona(i) {
        this.array_di.responsable.persona.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    addUnidad() {
        this.array_di.responsable.unidad.push({
            nombre: ''
        });
        console.log(this.array_di);
    }
    removeUnidad(i) {
        this.array_di.responsable.unidad.splice(i, 1); // elimina 1 indice a partir del indice i
        console.log(this.array_di);
    }
    addPubliRel() {
        this.array_di.publicaciones_relacionadas.push({
            titulo: '',
            url: ''
        });
        console.log(this.array_di);
    }
    removePubliRel(i) {
        this.array_di.publicaciones_relacionadas.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    addFile() {
        this.array_di.archivos.push({
            archivo: '',
            descripcion: '',
            fecha_embargo: ''
        });
        console.log(this.array_di);
    }
    removeFile(i) {
        this.array_di.archivos.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    cc(data) {
        if (data == "1") {
            this.titulo_cc = "Licencia Creative Commons Atribución 4.0 Internacional";
            this.url_cc = "https://creativecommons.org/licenses/by/4.0/legalcode.es";
            this.img_cc = "../../../assets/img/cc_40.png";
            this.img_licencia = "../../../assets/img/seal.png";
            this.descripcion_licencia = "¡Esta es una Licencia de Cultura Libre!";
            this.array_di.licencia_cc = this.titulo_cc;
        }
        if (data == "2") {
            this.url_cc = "https://creativecommons.org/licenses/by-sa/4.0/legalcode.es";
            this.titulo_cc = "Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional";
            this.img_cc = "../../../assets/img/cc_sa40.png";
            this.img_licencia = "../../../assets/img/seal.png";
            this.descripcion_licencia = "¡Esta es una Licencia de Cultura Libre!";
            this.array_di.licencia_cc = this.titulo_cc;
        }
        if (data == "3") {
            this.url_cc = "https://creativecommons.org/licenses/by-nd/4.0/legalcode.es";
            this.titulo_cc = "Licencia Creative Commons Atribución-SinDerivadas 4.0 Internacional.";
            this.img_cc = "../../../assets/img/cc_nd40.png";
            this.img_licencia = "../../../assets/img/no_aprueba.png";
            this.descripcion_licencia = "¡Esta no es una Licencia de Cultura Libre!";
            this.array_di.licencia_cc = this.titulo_cc;
        }
        if (data == "4") {
            this.titulo_cc = "Licencia Creative Commons Atribución-NoComercial 4.0 Internacional.";
            this.url_cc = "https://creativecommons.org/licenses/by-nc/4.0/legalcode.es";
            this.img_cc = "../../../assets/img/cc_nc_40.png";
            this.img_licencia = "../../../assets/img/no_aprueba.png";
            this.descripcion_licencia = "¡Esta no es una Licencia de Cultura Libre!";
            this.array_di.licencia_cc = this.titulo_cc;
        }
        if (data == "5") {
            this.url_cc = "https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.es";
            this.titulo_cc = "Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional.";
            this.img_cc = "../../../assets/img/cc_nc_sa40.png";
            this.img_licencia = "../../../assets/img/no_aprueba.png";
            this.descripcion_licencia = "¡Esta no es una Licencia de Cultura Libre!";
            this.array_di.licencia_cc = this.titulo_cc;
        }
        if (data == "6") {
            this.url_cc = "https://creativecommons.org/licenses/by-nd/4.0/legalcode.es";
            this.titulo_cc = "Licencia Creative Commons Atribución-SinDerivadas 4.0 Internacional.";
            this.img_cc = "../../../assets/img/cc_nd40.png";
            this.img_licencia = "../../../assets/img/no_aprueba.png";
            this.descripcion_licencia = "¡Esta no es una Licencia de Cultura Libre!";
            this.array_di.licencia_cc = this.titulo_cc;
        }
    }
    date() {
        var pruebaFecha = document.createElement("input");
        //Si tiene soporte: debe aceptar el tipo "date"...
        pruebaFecha.setAttribute("type", "date");
        if (pruebaFecha.type === "date") {
            this.soportadateInput = true;
        }
        else {
            this.soportadateInput = false;
        }
    }
    _handleReaderLoaded(readerEvent) {
        var binaryString = readerEvent.target.result;
        this.array_di.archivos[0].base64textString = btoa(binaryString);
    }
    seleccionarArchivo(event) {
        // nuevo
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.array_di.archivos[0].archivo = file.name;
            console.log(this.array_di);
        }
    }
    loguearse() {
        this.login = true;
        this.errLog = false;
    }
    closeLogin() {
        this.login = false;
    }
    // CARGA DI
    submitPublica(form) {
        console.log(this.array_di);
        /* console.log(this.array_Persona);
        console.log(this.array_Unidad);
        console.log(this.array_publi_relacionadas);
        console.log(this.array_file); */
        this.login = false;
        if (this.array_di.archivos[0].fecha_archivo == "0000-00-00") {
            var fecha = 0;
        }
        else {
            var fecha = this.array_di.archivos[0].fecha_archivo;
        }
        this.http.post(this.urlfile, this.array_di)
            .subscribe(response => {
            if (response['resultado'] == "200") {
                this.msj = "success";
                form.reset();
                this.array_di.archivos[0].archivo = null;
                this.array_di.archivos[0].fecha_embargo = "0000-00-00";
                this.array_di.concedo_licencia = false;
                console.log(response);
            }
            else if (response['resultado'] == "403") {
                this.msj = false;
                this.login = true;
                this.errLog = true;
                this.array_di.clave = null;
                console.log("se conectó pero login es incorrecto");
                console.log(response['resultado']);
            }
            else {
                this.msj = "error";
                form.reset();
                this.array_di.archivos[0].archivo = null;
                this.array_di.base64textString = null;
                this.array_di.concedo_licencia = false;
                console.log("se conectó pero no trajo resultado 200");
                console.log(response);
            }
        });
    }
    /*   submitPublica2(form){
          this.login = false;
          if(this.array_file[0].fecha_archivo == "0000-00-00"){
            var fecha:any = 0;
          }else{
            var fecha:any = this.array_file[0].fecha_archivo;
          }
          const formData = new FormData();
          
          formData.append('name', this.array_datos.autor);
          formData.append('titulo', this.array_datos.titulo);
          formData.append('resumen', this.array_datos.resumen);
          formData.append('fecha', fecha);
          formData.append('file', this.array_datos.fileSource);
          formData.append('user', localStorage.getItem("correo"));
          formData.append('clave', this.array_datos.clave);
          console.log(formData);
      
          this.http.post(this.urlfile, formData)
            .subscribe(response => {
      
              if(response['resultado'] == "200"){
                this.msj = "success";
                form.reset();
                this.array_datos.nombreArchivo= null;
                this.array_datos.fecha= "0000-00-00";
                this.array_datos.requerido= false;
                console.log(response);
                
              }else if(response['resultado']== "403"){
                this.msj = false;
                this.login = true;
                this.errLog = true;
                this.array_datos.clave = null;
                console.log("se conectó pero login es incorrecto");
                console.log(response['resultado'])
                
              }else{
                this.msj = "error";
                form.reset();
                this.array_datos.nombreArchivo= null;
                this.array_datos.base64textString= null;
                this.array_datos.requerido= false;
                console.log("se conectó pero no trajo resultado 200");
                console.log(response)
              }
            })
      
      } */
    resetFile() {
        this.array_di.archivos[0].fileSource = "";
        this.array_di.archivos[0].archivo = "";
    }
    newUpload(form) {
        this.msj = "";
        this.activePane = 't1';
    }
    toScroll() {
        document.getElementById('pasos').scrollIntoView({ behavior: 'smooth' });
    }
};
DatosInvestigacionComponent = tslib_1.__decorate([
    Component({
        selector: 'app-datos-investigacion',
        templateUrl: './datos-investigacion.component.html',
        styleUrls: ['./datos-investigacion.component.css'],
        animations: [
            trigger('slide', [
                state('t1', style({ transform: 'translateX(0)' })),
                state('t2', style({ transform: 'translateX(-25%)' })),
                state('t3', style({ transform: 'translateX(-50%)' })),
                state('t4', style({ transform: 'translateX(-75%)' })),
                transition('* => *', animate(300))
            ])
        ]
    })
], DatosInvestigacionComponent);
export { DatosInvestigacionComponent };
//# sourceMappingURL=datos-investigacion.component.js.map