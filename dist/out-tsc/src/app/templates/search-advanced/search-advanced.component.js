import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from './search-advanced.json';
import { global } from '../../services/global';
let SearchAdvancedComponent = class SearchAdvancedComponent {
    constructor(activatedRoute, queriesService, formBuilder, router) {
        this.activatedRoute = activatedRoute;
        this.queriesService = queriesService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.json = json;
        this.data = [];
        this.jsonFiltro = "";
        this.objFiltro = "";
        this.name_search_by = "";
        this.name_contains = "";
        this.name_term = "";
        this.valorSearch = "";
        this.fq = "";
        this.termino = "";
        this.operador = "";
        this.filtros = "";
        this.filtro = "";
        this.str_filtros = "";
        this.paginacion = [];
        this.urlPhp = global.php;
    }
    ngOnInit() {
        if (this.data['append_filter'] == undefined) {
            this.data['append_filter'] = this.json['horizon_search_form']['filters']['content'][0];
        }
        this.json['horizon_search_form']['filters']['content'].splice(1, 10);
        localStorage.clear();
        this.data['title'] = 'Búsqueda avanzada';
        // Recibo datos por get para pasarlos a funcion searchPage
        this.activatedRoute.params.subscribe(params => {
            this.data['param'] = params['search-term'];
            this.npage = parseInt(params['n-page']);
            this.searchPage(this.data['param'], this.npage);
        });
    }
    //Recibe de Búsqueda Simple y Avanzada, trae variable por url
    searchPage(param, npage) {
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '600px'
        }, 300);
        if (param == "home") {
            param = JSON.stringify({ "search_form": "", "search_by0": "", "contains0": "", "term0": "" });
        }
        this.objFiltro = JSON.parse(param);
        this.str_filtros = "";
        this.valorSearch = this.objFiltro['search_form'];
        this.data['title'] = 'Resultados de búsqueda';
        npage = (npage - 1) * 20;
        var j = (Object.keys(this.objFiltro).length - 1) / 3;
        var pos = 0;
        console.log(this.objFiltro);
        console.log(j);
        // Creo variables en localStorage
        localStorage.setItem('jsonFiltro', param);
        // Asigno variables de localStorage
        this.jsonFiltro = localStorage.getItem('jsonFiltro');
        // Creo variables en localStorage
        localStorage.setItem('search_form', this.valorSearch);
        // Asigno variables de localStorage
        this.objFiltro = JSON.parse(this.jsonFiltro);
        this.valorSearch = localStorage.getItem('search_form');
        for (let i = 0; i < j; i++) {
            localStorage.setItem('search_by' + i, this.objFiltro['search_by' + i]);
            localStorage.setItem('contains' + i, this.objFiltro['contains' + i]);
            localStorage.setItem('term' + i, this.objFiltro['term' + i]);
            pos++;
            this.fq = Object.values(this.objFiltro)[pos];
            this.fq = this.fq.replace(':', '');
            pos++;
            this.operador = Object.values(this.objFiltro)[pos];
            this.operador = this.operador.replace(':', '');
            pos++;
            this.termino = Object.values(this.objFiltro)[pos];
            this.termino = this.termino.replace(':', '');
            switch (this.fq) {
                case 'categoria':
                    this.filtro = "fq=dc.subject.dewey:" + this.termino + "%26";
                    break;
                case 'titulo':
                    if (this.operador == 'no-contiene') {
                        this.filtro = "fq=-title:" + this.termino + "%26";
                    }
                    else {
                        this.filtro = "fq=title:" + this.termino + "%26";
                    }
                    break;
                case 'autor':
                    if (this.operador == 'no-contiene') {
                        this.filtro = "fq=-author:" + this.termino + "%26";
                    }
                    else {
                        this.filtro = "fq=author:" + this.termino + "%26";
                    }
                    break;
                case 'fecha':
                    if (this.operador == 'no-contiene') {
                        this.filtro = "fq=-dateIssued:" + this.termino + "%26";
                    }
                    else {
                        this.filtro = "fq=dateIssued:" + this.termino + "%26";
                    }
                    break;
                case 'materia':
                    if (this.operador == 'no-contiene') {
                        this.filtro = "fq=-subject:" + this.termino + "%26";
                    }
                    else {
                        this.filtro = "fq=subject:" + this.termino + "%26";
                    }
                    break;
            }
            this.str_filtros = this.str_filtros + this.filtro;
        }
        this.urlFiltro = this.urlPhp + 'filtro=' + this.str_filtros + '&valor=' + this.valorSearch + '&start=' + npage;
        console.log(this.urlFiltro);
        this.queriesService.queryGet(this.urlFiltro)
            .then((data) => {
            this.data['search'] = Object.keys(data).map(i => data[i]);
            // filtros materia
            this.data['materia'] = this.data['search'][2]['facet_fields']['bi_4_dis_value_filter'];
            // filtros autores
            this.data['autores'] = this.data['search'][2]['facet_fields']['bi_2_dis_value_filter'];
            // filtros tipo documento
            this.data['tipodoc'] = this.data['search'][2]['facet_fields']['type_filter'];
            // PAGINACIÓN
            this.totalPage = this.data['search'][1]['numFound'];
            this.cantidadReg = 20;
            this.page = Math.ceil(this.totalPage / this.cantidadReg);
            console.log(this.data['search']);
            console.log(this.totalPage + " registros");
            console.log(this.page + " páginas");
            console.log('pagina cada ' + npage);
            console.log('página atual: ' + this.npage);
            this.paginacion = []; // Dejar vacío para volver a crear loop con cada consulta
            for (let i = 1; i <= this.page; i++) {
                if (i <= 5) {
                    if (this.npage > 5) {
                        this.paginacion.push(i + (this.npage - 5));
                    }
                    else {
                        this.paginacion.push(i);
                    }
                }
            }
            if (this.npage >= 2) {
                this.prevPage = this.npage - 1;
            }
            else {
                this.prevPage = 1;
            }
            if (this.npage < this.page) {
                this.nextPage = this.npage + 1;
            }
            else {
                this.nextPage = this.page;
            }
            // Fin paginación
            // MUESTRA/OCULTA SECCIÓN REGISTROS
            if (this.data['search'] == "") {
                this.registros = false;
            }
            else {
                this.registros = true;
            }
        });
        var indexfiltro = 0;
        this.createForm(indexfiltro, this.searchForm);
    }
    getSearch(form, $event) {
        $event.preventDefault();
        if (form.value) {
            this.filtros = JSON.stringify(form.value);
            this.router.navigate(['/search-advanced/' + this.filtros + '/1/']);
        }
    }
    addFilter(index, form) {
        this.json['horizon_search_form']['filters']['content'].push(this.data['append_filter']);
        localStorage.setItem('search_form', form.value.search_form);
        var num = index + 1;
        /* var search_by:any=this.searchForm.controls['search_by'+index].value;
        var contains:any=this.searchForm.controls['contains'+index].value;
        var term:any=this.searchForm.controls['term'+index].value;
        localStorage.setItem('search_by'+index, search_by);
        localStorage.setItem('contains'+index, contains);
        localStorage.setItem('term'+index, term); */
        this.createForm(index, this.searchForm);
    }
    removeFilter(index) {
        this.json['horizon_search_form']['filters']['content'].splice(index, 1);
        this.searchForm.controls['search_by' + index].setValue("");
        localStorage.setItem('search_by' + index, '');
        localStorage.removeItem('search_by' + index);
        this.searchForm.controls['contains' + index].setValue("");
        localStorage.setItem('contains' + index, '');
        localStorage.removeItem('contains' + index);
        this.searchForm.controls['term' + index].setValue("");
        localStorage.setItem('term' + index, '');
        localStorage.removeItem('term' + index);
    }
    createForm(index, searchForm) {
        //var index = index+1;
        alert(searchForm.controls['term0'].value);
        this.searchForm = this.formBuilder.group({
            search_form: (localStorage.getItem('search_form')),
            /* search_by0: this.searchForm.controls['search_by'+index].value,
            contains0: this.searchForm.controls['contains'+index].value,
            term0: searchForm.controls['term'+index].value),
            search_by1: this.searchForm.controls['search_by'+index].value,
            contains1: this.searchForm.controls['contains'+index].value,
            term1: this.searchForm.controls['term'+index].value, */
            search_by0: (localStorage.getItem('search_by2')),
            contains0: (localStorage.getItem('contains2')),
            term0: (searchForm.controls['term' + index].value),
            /* term0: ("a"), */
            search_by1: (localStorage.getItem('search_by2')),
            contains1: (localStorage.getItem('contains2')),
            term1: (localStorage.getItem('term2')),
            search_by2: (localStorage.getItem('search_by2')),
            contains2: (localStorage.getItem('contains2')),
            term2: (localStorage.getItem('term2')),
            search_by3: (localStorage.getItem('search_by3')),
            contains3: (localStorage.getItem('contains3')),
            term3: (localStorage.getItem('term3')),
            search_by4: (localStorage.getItem('search_by4')),
            contains4: (localStorage.getItem('contains4')),
            term4: (localStorage.getItem('term4')),
            search_by5: (localStorage.getItem('search_by5')),
            contains5: (localStorage.getItem('contains5')),
            term5: (localStorage.getItem('term5')),
            search_by6: (localStorage.getItem('search_by6')),
            contains6: (localStorage.getItem('contains6')),
            term6: (localStorage.getItem('term6')),
            search_by7: (localStorage.getItem('search_by7')),
            contains7: (localStorage.getItem('contains7')),
            term7: (localStorage.getItem('term7')),
            search_by8: (localStorage.getItem('search_by8')),
            contains8: (localStorage.getItem('contains8')),
            term8: (localStorage.getItem('term8')),
            search_by9: (localStorage.getItem('search_by9')),
            contains9: (localStorage.getItem('contains9')),
            term9: (localStorage.getItem('term9')),
            search_by10: (localStorage.getItem('search_by10')),
            contains10: (localStorage.getItem('contains10')),
            term10: (localStorage.getItem('term10'))
        });
    }
};
SearchAdvancedComponent = tslib_1.__decorate([
    Component({
        selector: 'app-search-advanced',
        templateUrl: './search-advanced.component.html',
        styleUrls: ['./search-advanced.component.css']
    })
], SearchAdvancedComponent);
export { SearchAdvancedComponent };
//# sourceMappingURL=search-advanced.component.js.map