import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoverSecondLevelComponent } from './cover-second-level.component';
const routes = [
    { path: '', component: CoverSecondLevelComponent }
];
let CoverSecondLevelRoutingModule = class CoverSecondLevelRoutingModule {
};
CoverSecondLevelRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], CoverSecondLevelRoutingModule);
export { CoverSecondLevelRoutingModule };
//# sourceMappingURL=cover-second-level-routing.module.js.map