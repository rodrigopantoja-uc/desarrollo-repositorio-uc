import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import json from '../../../assets/json/upload-record/upload-record.json';
import compartir from '../../../assets/json/upload-record/02-compartir-investigacion.json';
import { global } from '../../services/global';
let UploadRecordComponent = class UploadRecordComponent {
    constructor(uploadService, _queriesService, router, activatedRoute, http) {
        this.uploadService = uploadService;
        this._queriesService = _queriesService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.data = [];
        this.json = json;
        this.compartir = compartir;
        this.isLeftVisible = true;
        this.activePane = 't1';
        this.archivo = {
            autor: null,
            titulo: null,
            resumen: null,
            clave: this._queriesService.getPass(),
            user: localStorage.getItem("correo"),
            fecha: "0000-00-00",
            nombreArchivo: null,
            fileSource: null,
            base64textString: null,
            requerido: null
        };
        this.key = "5t1dd";
        this.urlfile = global.php + "/autoarchivo/subirArchivo.php";
        this.identity = this._queriesService.getIdentity();
        this.token = this._queriesService.getToken();
        this.password = this._queriesService.getPass();
    }
    ngOnInit() {
        // refresca página
        this._queriesService.getRefresh();
        this.date();
        // Después usar wards en routing
        /* if(!this.token){
          window.location.href='assets/php/cas-log/cas-login.php';
        } */
        this.usuario = localStorage.getItem('usuario');
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        //document.getElementById('arriba').scrollIntoView({behavior: 'smooth'});
    }
    ngDoCheck() {
        this.password = this._queriesService.getPass();
    }
    date() {
        var pruebaFecha = document.createElement("input");
        //Si tiene soporte: debe aceptar el tipo "date"...
        pruebaFecha.setAttribute("type", "date");
        if (pruebaFecha.type === "date") {
            this.soportadateInput = true;
        }
        else {
            this.soportadateInput = false;
        }
    }
    toScroll() {
        document.getElementById('pasos').scrollIntoView({ behavior: 'smooth' });
    }
    _handleReaderLoaded(readerEvent) {
        var binaryString = readerEvent.target.result;
        this.archivo.base64textString = btoa(binaryString);
    }
    seleccionarArchivo(event) {
        /*     var files = event.target.files;
            var file = files[0];
            this.archivo.nombreArchivo = file.name;
        
            if(files && file) {
              var reader = new FileReader();
              reader.onload = this._handleReaderLoaded.bind(this);
              reader.readAsBinaryString(file);
            } */
        // nuevo
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.archivo.fileSource = file;
            //console.log(file.name);
            console.log(file);
        }
    }
    resetFile() {
        this.archivo.fileSource = "";
        this.archivo.nombreArchivo = "";
    }
    // Nuevo carga
    submitPublica(form) {
        this.login = false;
        if (this.archivo.fecha == "0000-00-00") {
            var fecha = 0;
        }
        else {
            var fecha = this.archivo.fecha;
        }
        const formData = new FormData();
        formData.append('name', this.archivo.autor);
        formData.append('titulo', this.archivo.titulo);
        formData.append('resumen', this.archivo.resumen);
        formData.append('fecha', fecha);
        formData.append('file', this.archivo.fileSource);
        formData.append('user', localStorage.getItem("correo"));
        formData.append('clave', this.archivo.clave);
        console.log(formData);
        this.http.post(this.urlfile, formData)
            .subscribe(response => {
            if (response['resultado'] == "200") {
                this.msj = "success";
                form.reset();
                this.archivo.nombreArchivo = null;
                this.archivo.fecha = "0000-00-00";
                this.archivo.requerido = false;
                console.log(response);
            }
            else if (response['resultado'] == "403") {
                this.msj = false;
                this.login = true;
                this.errLog = true;
                this.archivo.clave = null;
                console.log("se conectó pero login es incorrecto");
                console.log(response['resultado']);
            }
            else {
                this.msj = "error";
                form.reset();
                this.archivo.nombreArchivo = null;
                this.archivo.base64textString = null;
                this.archivo.requerido = false;
                console.log("se conectó pero no trajo resultado 200");
                console.log(response);
            }
        });
    }
    // Carga antigua
    /*   submitPublica2(form){
        this.login = false;
    
        if(!this.password){
          this.archivo.clave = CryptoJS.AES.encrypt(this.archivo.clave.trim(), this.key.trim()).toString();
          console.log(this.archivo.clave);
          console.log(CryptoJS.AES.decrypt(this.archivo.clave.trim(), this.key.trim()).toString(CryptoJS.enc.Utf8));
        }
    
            this.uploadService.register(this.archivo).subscribe(
              response => {
                if(response.mensaje == "201"){
                  console.log(response)
                  this.msj = "success";
                  localStorage.setItem('password',this.archivo.clave);
                  form.reset();
                  this.archivo.nombreArchivo= null;
                  this.archivo.base64textString= null;
                  this.archivo.requerido= false;
                  
                }else if(response.mensaje == "403"){
                  this.msj = false;
                  this.login = true;
                  this.errLog = true;
                  this.archivo.clave = null;
                  console.log("se conectó pero login es incorrecto");
                  console.log(response.mensaje)
                }else{
                  this.msj = "error";
                  console.log("se conectó pero no trajo resultado 201");
                  console.log(response)
                  form.reset();
                  this.archivo.nombreArchivo= null;
                  this.archivo.base64textString= null;
                  this.archivo.requerido= false;
                }
              },
              error =>{
                this.msj = "error";
                console.log(<any>error);
                console.log("No se conectó con la api");
                form.reset();
                  this.archivo.nombreArchivo= null;
                  this.archivo.base64textString= null;
                  this.archivo.requerido= false;
              }
            );
    
      } */
    loguearse() {
        this.login = true;
        this.errLog = false;
    }
    closeLogin() {
        this.login = false;
    }
    modal() {
        var modal = document.getElementById("tvesModal");
        var body = document.getElementsByTagName("body")[0];
        modal.style.display = "block";
        body.style.position = "static";
        body.style.height = "100%";
        body.style.overflow = "hidden";
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
                body.style.position = "inherit";
                body.style.height = "auto";
                body.style.overflow = "visible";
            }
        };
    }
    closeModal() {
        var modal = document.getElementById("tvesModal");
        var body = document.getElementsByTagName("body")[0];
        modal.style.display = "none";
        body.style.position = "inherit";
        body.style.height = "auto";
        body.style.overflow = "visible";
    }
    newUpload(form) {
        this.msj = "";
        this.activePane = 't1';
    }
};
UploadRecordComponent = tslib_1.__decorate([
    Component({
        selector: 'app-upload-record',
        templateUrl: './upload-record.component.html',
        styleUrls: ['./upload-record.component.css'],
        //changeDetection: ChangeDetectionStrategy.OnPush,
        animations: [
            trigger('slide', [
                state('t1', style({ transform: 'translateX(0)' })),
                state('t2', style({ transform: 'translateX(-25%)' })),
                state('t3', style({ transform: 'translateX(-50%)' })),
                state('t4', style({ transform: 'translateX(-75%)' })),
                transition('* => *', animate(300))
            ])
        ]
    })
], UploadRecordComponent);
export { UploadRecordComponent };
//# sourceMappingURL=upload-record.component.js.map