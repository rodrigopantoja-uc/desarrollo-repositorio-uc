import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadRecordRoutingModule } from './upload-record-routing.module';
import { UploadRecordComponent } from './upload-record.component';
/* import { BreadcrumbsComponent } from '../../partials/commons/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from '../../partials/commons/title/title.component'; */
let UploadRecordModule = class UploadRecordModule {
};
UploadRecordModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            UploadRecordComponent
            /*     BreadcrumbsComponent,
                TitleComponent */
        ],
        imports: [
            CommonModule,
            UploadRecordRoutingModule
        ]
    })
], UploadRecordModule);
export { UploadRecordModule };
//# sourceMappingURL=upload-record.module.js.map