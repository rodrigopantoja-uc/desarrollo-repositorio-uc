import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import { ApisService } from '../../services/apis.service';
let ContactModule = class ContactModule {
};
ContactModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            ContactComponent
        ],
        imports: [
            CommonModule,
            ContactRoutingModule
        ],
        providers: [
            ApisService
        ]
    })
], ContactModule);
export { ContactModule };
//# sourceMappingURL=contact.module.js.map