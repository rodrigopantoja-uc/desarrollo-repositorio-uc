import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusquedaRoutingModule } from './busqueda-routing.module';
import { BusquedaComponent } from './busqueda.component';
import { CardPublicationsComponent } from '../../partials/cards/card-publications/card-publications.component';
let BusquedaModule = class BusquedaModule {
};
BusquedaModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            BusquedaComponent,
            CardPublicationsComponent
        ],
        imports: [
            CommonModule,
            BusquedaRoutingModule
        ]
    })
], BusquedaModule);
export { BusquedaModule };
//# sourceMappingURL=busqueda.module.js.map