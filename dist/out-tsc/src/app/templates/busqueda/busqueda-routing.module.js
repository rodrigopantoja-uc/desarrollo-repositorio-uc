import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BusquedaComponent } from './busqueda.component';
const routes = [
    { path: '', component: BusquedaComponent }
];
let BusquedaRoutingModule = class BusquedaRoutingModule {
};
BusquedaRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], BusquedaRoutingModule);
export { BusquedaRoutingModule };
//# sourceMappingURL=busqueda-routing.module.js.map