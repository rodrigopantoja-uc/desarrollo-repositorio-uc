import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from './cover-first-level.json';
import busqueda_tesis from './05-busqueda-tesis.json';
import compartir from './02-compartir-investigacion.json';
let CoverFirstLevelComponent = class CoverFirstLevelComponent {
    constructor(queriesService, activatedRoute, router) {
        this.queriesService = queriesService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.data = [];
        this.main = json;
        this.busqueda_tesis = busqueda_tesis;
        this.compartir = compartir;
    }
    ngOnInit() {
        this.queriesService.queryGet('http://localhost:3000/topics').then((data) => { this.data['topics'] = data; });
        this.queriesService.queryGet('http://localhost:3000/publics').then((data) => { this.data['publics'] = data; });
        this.activatedRoute.params.subscribe(params => {
            this.pagina = params['pag'];
        });
    }
    ngDoCheck() {
        this.main = json;
        this.paginas();
    }
    paginas() {
        if (this.pagina == "tesis") {
            this.main = this.main['tesis'];
        }
        if (this.pagina == "publicaciones") {
            this.main = this.main['publicaciones'];
        }
        if (this.pagina == "enlaces-recursos") {
            this.main = this.main['enlaces-recursos'];
        }
        if (this.pagina == "que-es-repositorio") {
            this.main = this.main[this.pagina];
        }
    }
};
CoverFirstLevelComponent = tslib_1.__decorate([
    Component({
        selector: 'app-cover-first-level',
        templateUrl: './cover-first-level.component.html',
        styleUrls: ['./cover-first-level.component.css']
    })
], CoverFirstLevelComponent);
export { CoverFirstLevelComponent };
//# sourceMappingURL=cover-first-level.component.js.map