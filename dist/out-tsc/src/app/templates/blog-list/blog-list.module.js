import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogListRoutingModule } from './blog-list-routing.module';
import { BlogListComponent } from './blog-list.component';
let BlogListModule = class BlogListModule {
};
BlogListModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            BlogListComponent,
        ],
        imports: [
            CommonModule,
            BlogListRoutingModule
        ]
    })
], BlogListModule);
export { BlogListModule };
//# sourceMappingURL=blog-list.module.js.map