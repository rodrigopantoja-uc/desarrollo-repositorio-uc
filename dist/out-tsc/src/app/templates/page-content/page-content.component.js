import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { global } from '../../services/global';
import PoliticasAccesoAbierto from '../../../assets/json/template-content/politicas-de-acceso-abierto.json';
import QuienesSomos from '../../../assets/json/template-content/quienes-somos.json';
import NuestroEquipo from '../../../assets/json/template-content/nuestro-equipo.json';
import PoliticasPrivacidad from '../../../assets/json/template-content/politicas-de-privacidad.json';
import Licencia from '../../../assets/json/template-content/licencia.json';
import CondicionesUso from '../../../assets/json/template-content/condiciones-de-uso.json';
import MapaSitio from '../../../assets/json/template-content/mapa-del-sitio.json';
import SubidosRecientemente from '../../../assets/json/template-content/subidos-recientemente.json';
import MasDescargados from '../../../assets/json/template-content/mas-descargados.json';
import Destacados from '../../../assets/json/template-content/destacados.json';
import InvestigacionMes from '../../../assets/json/template-content/investigacion-del-mes.json';
import Suscripcion from '../../../assets/json/template-content/suscripcion.json';
import json from '../../../assets/json/home.json';
let PageContentComponent = class PageContentComponent {
    constructor(subscribeService, _sanitizer, queriesService, activatedRoute, router) {
        this.subscribeService = subscribeService;
        this._sanitizer = _sanitizer;
        this.queriesService = queriesService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.data = [];
        this.json = json;
        this.subscribeData = {};
        this.urlPhp = global.php;
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        // SUSCRIBETE A NUESTRO NEWSLETTER
        this.data['SuscripcionNews'] = json['horizon_novedades']['section_suscription'];
        // NOVEDADES NEWS MAILCHIMP
        this.queriesService.queryGet(this.urlPhp + '/api-mailchimp/landing-page.php').then((data) => {
            this.data['news'] = data['landing_pages'].reverse();
            console.log(this.data['news']);
        });
        this.activatedRoute.params.subscribe(params => {
            this.pagina = params['pag'];
            // Scroll
            document.getElementById('banner').scrollIntoView({ behavior: 'smooth' });
            this.postend();
            // JQuery ir arriba
            /* $('body, html').animate({
              scrollTop: '0px'
            }, 300); */
        });
    }
    ngDoCheck() {
        this.paginas();
    }
    paginas() {
        // MENU NOVEDADES
        if (this.pagina == "subidos-recientemente") {
            this.data['content'] = SubidosRecientemente;
        }
        if (this.pagina == "mas-descargados") {
            this.data['content'] = MasDescargados;
        }
        if (this.pagina == "destacados") {
            this.data['content'] = Destacados;
        }
        if (this.pagina == "investigacion-del-mes") {
            this.data['content'] = InvestigacionMes;
        }
        if (this.pagina == "suscripcion") {
            this.data['content'] = Suscripcion;
        }
        // MENU QUE ES REPOSITORIO
        if (this.pagina == "politicas-de-acceso-abierto") {
            this.data['content'] = PoliticasAccesoAbierto;
        }
        if (this.pagina == "quienes-somos") {
            this.data['content'] = QuienesSomos;
        }
        if (this.pagina == "nuestro-equipo") {
            this.data['content'] = NuestroEquipo;
        }
        if (this.pagina == "politicas-de-privacidad") {
            this.data['content'] = PoliticasPrivacidad;
        }
        if (this.pagina == "condiciones-de-uso") {
            this.data['content'] = CondicionesUso;
        }
        if (this.pagina == "mapa-del-sitio") {
            this.data['content'] = MapaSitio;
        }
        if (this.pagina == "licencia") {
            this.data['content'] = Licencia;
        }
    }
    postend() {
        /* 10 últimas publicaciones  */
        this.queriesService.queryGet(this.urlPhp + "/ultimos-agregados.php")
            .then((data) => {
            this.data['search'] = Object.keys(data).map(i => data[i]);
            this.data['search'] = this.data['search'][1]['docs'];
        });
    }
    newsletter() {
        this.queriesService.queryGet(this.urlPhp + "/api-mailchimp/suscribir-newsletter.php?email=" + this.subscribeData['email'])
            .then((data) => {
            /* this.data['search']= Object.keys(data) .map(i =>data[i]);
            this.data['search']= this.data['search'][1]['docs']; */
            //console.log(data);
            //console.log(this.subscribeData['email']);
            if (data['status'] == 200) {
                this.status = "success";
                //console.log(data['mensaje']);
            }
            if (data['status'] == 214) {
                this.status = "existe";
                //console.log(data['mensaje']);
            }
            if (data['status'] == 400) {
                this.status = "error";
                //console.log(data['mensaje']);
            }
        });
    }
    volverSuscribir() {
        this.status = false;
        //alert("aaa");
    }
    /*   subscribe(form) {
    
        this.subscribeService.subscribeToList(this.subscribeData)
          .subscribe(res => {
            this.status = "success";
          }, err => {
            console.log(err);
          })
      } */
    getBackgroundImage(image) {
        return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
    }
};
PageContentComponent = tslib_1.__decorate([
    Component({
        selector: 'app-page-content',
        templateUrl: './page-content.component.html',
        styleUrls: ['./page-content.component.css']
    })
], PageContentComponent);
export { PageContentComponent };
//# sourceMappingURL=page-content.component.js.map