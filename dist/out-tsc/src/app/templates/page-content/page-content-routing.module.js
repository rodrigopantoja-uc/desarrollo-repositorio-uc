import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PageContentComponent } from './page-content.component';
const routes = [
    { path: '', component: PageContentComponent }
];
let PageContentRoutingModule = class PageContentRoutingModule {
};
PageContentRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], PageContentRoutingModule);
export { PageContentRoutingModule };
//# sourceMappingURL=page-content-routing.module.js.map