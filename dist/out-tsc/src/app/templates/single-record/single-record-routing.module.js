import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SingleRecordComponent } from './single-record.component';
const routes = [
    { path: '', component: SingleRecordComponent }
];
let SingleRecordRoutingModule = class SingleRecordRoutingModule {
};
SingleRecordRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], SingleRecordRoutingModule);
export { SingleRecordRoutingModule };
//# sourceMappingURL=single-record-routing.module.js.map