import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoverContentComponent } from './cover-content.component';
const routes = [
    { path: '', component: CoverContentComponent }
];
let CoverContentRoutingModule = class CoverContentRoutingModule {
};
CoverContentRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], CoverContentRoutingModule);
export { CoverContentRoutingModule };
//# sourceMappingURL=cover-content-routing.module.js.map