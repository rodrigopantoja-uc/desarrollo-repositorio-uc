import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from '../../../assets/json/cover-content/enlaces.json';
let CoverContentComponent = class CoverContentComponent {
    constructor(activatedRoute, router, queriesService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.queriesService = queriesService;
        this.data = [];
        this.main = json;
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        this.activatedRoute.params.subscribe(params => {
            this.pagina = params['pag'];
            // JQuery ir arriba
            $('body, html').animate({
                scrollTop: '0px'
            }, 300);
        });
    }
    ngDoCheck() {
        this.main = json;
        this.paginas();
    }
    paginas() {
        // MENU ENLACES Y RECURSOS
        if (this.pagina == "otros-repositorios") {
            this.main = this.main[this.pagina];
        }
        if (this.pagina == "recursos-uc") {
            this.main = this.main[this.pagina];
        }
        if (this.pagina == "datos-investigacion") {
            this.main = this.main[this.pagina];
        }
        if (this.pagina == "patentes-uc") {
            this.main = this.main[this.pagina];
        }
        if (this.pagina == "sobre-compartir-datos") {
            this.main = this.main[this.pagina];
        }
    }
};
CoverContentComponent = tslib_1.__decorate([
    Component({
        selector: 'app-cover-content',
        templateUrl: './cover-content.component.html',
        styleUrls: ['./cover-content.component.css']
    })
], CoverContentComponent);
export { CoverContentComponent };
//# sourceMappingURL=cover-content.component.js.map