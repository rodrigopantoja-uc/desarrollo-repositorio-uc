import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoverContentRoutingModule } from './cover-content-routing.module';
import { CoverContentComponent } from './cover-content.component';
let CoverContentModule = class CoverContentModule {
};
CoverContentModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            CoverContentComponent
        ],
        imports: [
            CommonModule,
            CoverContentRoutingModule
        ]
    })
], CoverContentModule);
export { CoverContentModule };
//# sourceMappingURL=cover-content.module.js.map