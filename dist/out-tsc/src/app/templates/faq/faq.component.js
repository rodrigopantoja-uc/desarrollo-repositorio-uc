import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from '../../../assets/json/faq/faq.json';
import Faq from '../../../assets/json/faq/preguntas-frecuentes.json';
let FaqComponent = class FaqComponent {
    constructor(activatedRoute, router, queriesService, dynamicScriptLoader) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.queriesService = queriesService;
        this.dynamicScriptLoader = dynamicScriptLoader;
        this.data = [];
        this.json = json;
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        document.getElementById('arriba').scrollIntoView({ behavior: 'smooth' });
        this.data['param'] = this.activatedRoute.snapshot.paramMap.get('search-term');
        if (this.data['param'] == null) {
            this.data['faqs'] = Faq['preguntas-frecuente'];
            //Carga dinámica del script del Kit UC por conflictos en tiempo de ejecución con Angular
            this.dynamicScriptLoader.load('uc-kit');
        }
        else {
            this.data['faqs'] = Faq['preguntas-frecuente'];
            //Carga dinámica del script del Kit UC por conflictos en tiempo de ejecución con Angular
            this.dynamicScriptLoader.load('uc-kit');
            this.queriesService.queryGet('http://localhost:3000/faqsearch').then((data) => {
                this.data['faqs'] = data;
                //console.log(data);
                //console.log("2");
                //Carga dinámica del script del Kit UC por conflictos en tiempo de ejecución con Angular
                this.dynamicScriptLoader.load('uc-kit');
            }, (error) => { console.log(error); });
        }
    }
    filterFaqs(form, $event) {
        $event.preventDefault();
        if (form.form.value) {
            window.location.href = '/faq/search';
            // this.router.navigateByUrl('/faq/search');
        }
    }
};
FaqComponent = tslib_1.__decorate([
    Component({
        selector: 'app-faq',
        templateUrl: './faq.component.html',
        styleUrls: ['./faq.component.css']
    })
], FaqComponent);
export { FaqComponent };
//# sourceMappingURL=faq.component.js.map