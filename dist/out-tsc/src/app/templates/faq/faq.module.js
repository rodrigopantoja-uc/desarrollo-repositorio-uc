import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqRoutingModule } from './faq-routing.module';
import { FaqComponent } from './faq.component';
let FaqModule = class FaqModule {
};
FaqModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            FaqComponent
        ],
        imports: [
            CommonModule,
            FaqRoutingModule
        ]
    })
], FaqModule);
export { FaqModule };
//# sourceMappingURL=faq.module.js.map