import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from '../../../assets/json/bibliometrics.json';
import Studies from '../../../assets/json/studies.json';
let BibliometricsComponent = class BibliometricsComponent {
    constructor(queriesService) {
        this.queriesService = queriesService;
        this.data = [];
        this.mainBibliometrics = {};
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        this.mainBibliometrics = json;
        this.data['studies'] = Studies;
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    }
    scrollTo(element, $event) {
        $event.preventDefault();
        document.getElementById(element).scrollIntoView({ behavior: 'smooth' });
    }
};
BibliometricsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-bibliometrics',
        templateUrl: './bibliometrics.component.html',
        styleUrls: ['./bibliometrics.component.css']
    })
], BibliometricsComponent);
export { BibliometricsComponent };
//# sourceMappingURL=bibliometrics.component.js.map