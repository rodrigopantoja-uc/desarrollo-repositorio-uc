import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BibliometricsRoutingModule } from './bibliometrics-routing.module';
import { BibliometricsComponent } from './bibliometrics.component';
import { CardBibliometricsComponent } from '../../partials/cards/card-bibliometrics/card-bibliometrics.component';
let BibliometricsModule = class BibliometricsModule {
};
BibliometricsModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            BibliometricsComponent,
            CardBibliometricsComponent
        ],
        exports: [],
        imports: [
            CommonModule,
            BibliometricsRoutingModule
        ]
    })
], BibliometricsModule);
export { BibliometricsModule };
//# sourceMappingURL=bibliometrics.module.js.map