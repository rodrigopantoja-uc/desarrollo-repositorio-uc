import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from './profile-researcher.json';
let ProfileResearcherComponent = class ProfileResearcherComponent {
    constructor(queriesService) {
        this.queriesService = queriesService;
        this.data = [];
        this.json = json;
    }
    ngOnInit() {
        this.queriesService.queryGet('http://localhost:3000/users?id=00000001').then((data) => { this.data['user'] = data[0]; });
        this.queriesService.queryGet('http://localhost:3000/topics').then((data) => { this.data['topics'] = data; });
        this.queriesService.queryGet('http://localhost:3000/publics').then((data) => {
            this.data['publics'] = data;
            console.log(this.data['publics']);
        });
    }
    printProfile(dataPrint) {
        let printContents = dataPrint.innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
};
ProfileResearcherComponent = tslib_1.__decorate([
    Component({
        selector: 'app-profile-researcher',
        templateUrl: './profile-researcher.component.html',
        styleUrls: ['./profile-researcher.component.css']
    })
], ProfileResearcherComponent);
export { ProfileResearcherComponent };
//# sourceMappingURL=profile-researcher.component.js.map