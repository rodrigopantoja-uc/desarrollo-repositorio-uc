import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
let IdentityGuard = class IdentityGuard {
    constructor(_queriesService, _router) {
        this._queriesService = _queriesService;
        this._router = _router;
    }
    canActivate() {
        let identity = this._queriesService.getToken();
        if (identity) {
            return true;
        }
        else {
            window.location.href = 'assets/php/cas-log/cas-login.php?ruta=upload/subir-publicacion';
            return false;
        }
    }
};
IdentityGuard = tslib_1.__decorate([
    Injectable()
], IdentityGuard);
export { IdentityGuard };
//# sourceMappingURL=identity.guard.js.map