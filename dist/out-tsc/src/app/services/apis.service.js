import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
let ApisService = class ApisService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    /**
    *   nombre: queryGet
    *   tipo: function
    *   parametros: url (URL de una API)
    *   return: Promise (Contiene la respuesta de la API)
    *   descripcion: Recibe la URL de una API, ejecuta un GET nativo de Angular, cuyo resultado es retornado en forma de Promise
    */
    queryGet(url) {
        return new Promise((resolve, reject) => {
            this.httpClient.get(url).pipe(map((res) => res)).subscribe((data) => {
                resolve(data);
            }, (err) => {
                console.log("error desde el servicio");
                console.log(err);
                reject();
            });
        });
    }
    /* DATOS DEL USUARIO LOGEADO */
    getIdentity() {
        let identity = JSON.parse(localStorage.getItem("identity")); // Traigo json y lo paso a objeto
        if (identity && identity != "undefined") {
            this.identity = identity;
        }
        else {
            this.identity = null;
        }
        return this.identity;
    }
    /* TOKEN SI ESTÁ LOGUEADO */
    getToken() {
        let token = localStorage.getItem("correo"); // Traigo string token
        if (token && token != "undefined") {
            this.token = token;
        }
        else {
            this.token = null;
        }
        return this.token;
    }
    /* CLAVE DEL USUARIO LOGEADO */
    getPass() {
        let pass = localStorage.getItem("password"); // Traigo json y lo paso a objeto
        if (pass && pass != "undefined") {
            this.pass = pass;
        }
        else {
            this.pass = null;
        }
        return this.pass;
    }
    /**
    *   nombre: queryPost
    *   tipo: function
    *   parametros: url (URL de una API), parameters (Datos necesarios para la Query)
    *   return: Promise (Contiene la respuesta de la API)
    *   descripcion: Recibe la URL de una API y los parámetros de la query, ejecuta un POST nativo de Angular, cuyo resultado es retornado en forma de Promise
    */
    queryPost(url, parameters) {
        return new Promise((resolve, reject) => {
            this.httpClient.post(url, parameters).pipe(map((res) => res)).subscribe((data) => {
                resolve(data);
            }, (err) => {
                console.log(err);
                reject();
            });
        });
    }
    /**
    *   nombre: queryDelete
    *   tipo: function
    *   parametros: url (URL de una API)
    *   return: Promise (Contiene la respuesta de la API)
    *   descripcion: Recibe la URL de una API y los parámetros de la query, ejecuta un DELETE nativo de Angular, cuyo resultado es retornado en forma de Promise
    */
    queryDelete(url) {
        return new Promise((resolve, reject) => {
            this.httpClient.delete(url).pipe(map((res) => res)).subscribe((data) => {
                resolve(data);
            }, (err) => {
                console.log(err);
                reject();
            });
        });
    }
    /**
    *   nombre: queryPut
    *   tipo: function
    *   parametros: url (URL de una API)
    *   return: Promise (Contiene la respuesta de la API)
    *   descripcion: Recibe la URL de una API y parametros, ejecuta un PUT nativo de Angular, cuyo resultado es retornado en forma de Promise
    */
    queryPut(url, parameters) {
        return new Promise((resolve, reject) => {
            this.httpClient.put(url, parameters).pipe(map((res) => res)).subscribe((data) => {
                resolve(data);
            }, (err) => {
                console.log(err);
                reject();
            });
        });
    }
};
ApisService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], ApisService);
export { ApisService };
//# sourceMappingURL=apis.service.js.map