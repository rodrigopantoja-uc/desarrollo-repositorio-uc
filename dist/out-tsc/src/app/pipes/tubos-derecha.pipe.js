import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
let TubosDerechaPipe = class TubosDerechaPipe {
    /* transform(value:string, [separator]):string {
      let splits = value.split(separator);
      if(splits.length > 1) {
        return splits.pop();
      } else {
        return '';
      }
    } */
    transform(value) {
        let splits = value.split('|||');
        if (splits.length > 1) {
            return splits.pop();
        }
        else {
            return value;
        }
    }
};
TubosDerechaPipe = tslib_1.__decorate([
    Pipe({
        name: "tubos_derecha"
    })
], TubosDerechaPipe);
export { TubosDerechaPipe };
//# sourceMappingURL=tubos-derecha.pipe.js.map