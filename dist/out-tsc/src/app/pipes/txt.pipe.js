import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
let TxtPipe = class TxtPipe {
    transform(value) {
        let splits = value.replace('.txt', '');
        return splits;
    }
};
TxtPipe = tslib_1.__decorate([
    Pipe({
        name: "txt"
    })
], TxtPipe);
export { TxtPipe };
//# sourceMappingURL=txt.pipe.js.map