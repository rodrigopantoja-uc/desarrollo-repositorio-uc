import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
let FechaPipe = class FechaPipe {
    transform(value, limite) {
        let limit = parseInt(limite);
        //console.log(value.length > limit ? value.substring(0,limit) :   value);
        return value.length > limit ? value.substring(0, limit) : value;
    }
};
FechaPipe = tslib_1.__decorate([
    Pipe({
        name: "fecha"
    })
], FechaPipe);
export { FechaPipe };
//# sourceMappingURL=fecha.pipe.js.map