import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
let SlashIzquierdaPipe = class SlashIzquierdaPipe {
    transform(value) {
        let splits = value.split('/');
        if (splits.length > 1) {
            return splits[0];
        }
        else {
            return '';
        }
    }
};
SlashIzquierdaPipe = tslib_1.__decorate([
    Pipe({
        name: "slash_izquierda"
    })
], SlashIzquierdaPipe);
export { SlashIzquierdaPipe };
//# sourceMappingURL=slash-izquierda.pipe.js.map