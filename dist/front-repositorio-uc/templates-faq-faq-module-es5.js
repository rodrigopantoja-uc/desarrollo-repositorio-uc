function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["templates-faq-faq-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/faq/faq.component.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/templates/faq/faq.component.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppTemplatesFaqFaqComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- Google Tag Manager (noscript) -->\n<!-- <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-38GZNP\"\n  height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript> -->\n<!-- End Google Tag Manager (noscript) -->\n\n<section class=\"uc-section uc-section__header mb-60\" id=\"arriba\">\n  <div class=\"container\">\n    <!-- partials/commons/breadcrumbs -->\n    <!-- <app-breadcrumbs [breadcrumbs]=\"json['horizon_header']['section_breadcrumbs']['breadcrumbs']\" *ngIf=\"json['horizon_header']['section_breadcrumbs']['active']\"></app-breadcrumbs> -->\n    \n    <ul class=\"uc-breadcrumb mb-24\" *ngIf=\"json['horizon_header']['section_breadcrumbs']['active']\">\n      <li *ngFor=\"let breadcrumb of json['horizon_header']['section_breadcrumbs']['breadcrumbs']; last as l\" class=\"uc-breadcrumb_item{{(l) ? ' current' : '' }}\">\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"!breadcrumb['dynamic']\">{{ breadcrumb.text }}</a>\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"breadcrumb['dynamic']\">{{ particular }}</a>\n        <i class=\"uc-icon\" *ngIf=\"!l\">keyboard_arrow_right</i>\n      </li>\n    </ul>\n\n    <h1>{{ json['horizon_header']['title'] }}</h1>\n    <p class=\"mt-2\">Si tienes consultas más específicas, <a href=\"/contact\">puedes contactarnos aquí.</a></p>\n  </div>\n</section>\n\n<section class=\"uc-section uc-section__page pb-60\">\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <!-- FILTROS -->\n      <!-- <div class=\"col-md-4 col-6\" *ngIf=\"!json['horizon_filters']['active']\">\n        <div class=\"uc-card\" *ngIf=\"data['faqs']\">\n          <div class=\"uc-card_body\">\n            <form (ngSubmit)=\"filterFaqs(form, $event)\" #form=\"ngForm\">\n\n              <div class=\"uc-form-group\" *ngIf=\"json['horizon_filters']['section_search']['active']\">\n                <input id=\"{{ json['horizon_filters']['section_search']['name'] }}\" name=\"{{ json['horizon_filters']['section_search']['name'] }}\" type=\"text\" class=\"uc-input-style w-icon search\" placeholder=\"{{ json['horizon_filters']['section_search']['placeholder'] }}\" ngModel>\n                <span class=\"w-icon search\">\n                  <i class=\"uc-icon\">{{ json['horizon_filters']['section_search']['icon'] }}</i>\n                </span>\n              </div>\n              <ng-container *ngIf=\"json['horizon_filters']['section_filters']['active']\">\n                <span class=\"uc-subtitle mb-12\">{{ json['horizon_filters']['section_filters']['title'] }}</span>\n                <div class=\"uc-form-group mb-0 mt-12 d-flex align-items--center\" *ngFor=\"let topic of data['faqs']['topics']\">\n                  <input id=\"{{ topic['slug'] }}\" name=\"{{ topic['slug'] }}\" type=\"checkbox\" class=\"uc-input__checkbox mr-8\" value=\"{{ topic['slug'] }}\" ngModel>\n                  <label for=\"{{ topic['slug'] }}\" class=\"mb-0 text-weight--500 text-size--14 line-height--1\">{{ topic['name'] }}</label>\n                </div>\n              </ng-container>\n              <button title=\"{{ json['horizon_filters']['section_button']['title'] }}\" type=\"submit\" class=\"uc-btn btn-primary mt-24 w-100\" *ngIf=\"json['horizon_filters']['section_button']['active']\">\n                {{ json['horizon_filters']['section_button']['text'] }}\n                <i class=\"uc-icon icon-shape--rounded\">{{ json['horizon_filters']['section_button']['icon'] }}</i>\n              </button>\n            </form>\n          </div>\n        </div>\n\n      </div> -->\n\n      <!-- MUESTRA LISTA FAQS-->\n      <div class=\"col-md-12 col-12\" *ngIf=\"json['horizon_faqs']['active']\">\n        <div class=\"uc-accordion\" data-accordion *ngIf=\"data['faqs']\">\n\n          <div class=\"uc-collapse uc-card\" *ngFor=\"let faqs of data['faqs']; let i = index\">\n            <a class=\"uc-collapse_heading\" [attr.data-collapse]=\"'collapse_'+i\" title=\"{{faqs['title']}}\">\n              <i class=\"uc-icon mr-16\" style=\"width: 11px;\">book</i>\n              <h2 class=\"text-size--20 text-weight--700 col col-md-8\">\n                {{faqs[\"pregunta\"]}}\n              </h2>\n              <span class=\"uc-btn btn-inline ml-auto\">\n                Ver más <i class=\"uc-icon icon-shape--rounded\">add</i>\n              </span>\n            </a>\n            <div class=\"uc-collapse_body\" [attr.data-toggle]=\"'collapse_'+i\">\n              <table class=\"uc-table uc-table--faq\">\n                <tbody>\n                  <!-- <tr *ngFor=\"let faqs of data['faqs']\"> -->\n                  <tr> \n                    <td>\n                      <!-- <h3 class=\"text-size--16\">{{ faqs['pregunta'] }}</h3> -->\n                      <p [innerHTML]=\"faqs['respuesta']\"></p>\n                      <div *ngIf=\" faqs['sub-preguntas']\">\n                        <div *ngFor=\"let sub of faqs['sub-preguntas']\">\n                          <h3>{{ sub['pregunta'] }}</h3>\n                          <p >{{ sub['respuesta'] }}</p>\n                        </div>\n                      </div>\n                    </td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n";
    /***/
  },

  /***/
  "./src/app/services/dynamic-script-loader.service.ts":
  /*!***********************************************************!*\
    !*** ./src/app/services/dynamic-script-loader.service.ts ***!
    \***********************************************************/

  /*! exports provided: ScriptStore, DynamicScriptLoaderService */

  /***/
  function srcAppServicesDynamicScriptLoaderServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScriptStore", function () {
      return ScriptStore;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DynamicScriptLoaderService", function () {
      return DynamicScriptLoaderService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ScriptStore = [{
      name: 'uc-kit',
      src: '../../assets/js/uc-kit.js'
    }];

    var DynamicScriptLoaderService =
    /*#__PURE__*/
    function () {
      function DynamicScriptLoaderService() {
        var _this = this;

        _classCallCheck(this, DynamicScriptLoaderService);

        this.scripts = {};
        ScriptStore.forEach(function (script) {
          _this.scripts[script.name] = {
            loaded: false,
            src: script.src
          };
        });
      }

      _createClass(DynamicScriptLoaderService, [{
        key: "load",
        value: function load() {
          var _this2 = this;

          var promises = [];

          for (var _len = arguments.length, scripts = new Array(_len), _key = 0; _key < _len; _key++) {
            scripts[_key] = arguments[_key];
          }

          scripts.forEach(function (script) {
            return promises.push(_this2.loadScript(script));
          });
          return Promise.all(promises);
        }
      }, {
        key: "loadScript",
        value: function loadScript(name) {
          var _this3 = this;

          return new Promise(function (resolve, reject) {
            if (!_this3.scripts[name].loaded) {
              //load script
              var script = document.createElement('script');
              script.type = 'text/javascript';
              script.src = _this3.scripts[name].src;

              if (script.readyState) {
                //IE
                script.onreadystatechange = function () {
                  if (script.readyState === "loaded" || script.readyState === "complete") {
                    script.onreadystatechange = null;
                    _this3.scripts[name].loaded = true;
                    resolve({
                      script: name,
                      loaded: true,
                      status: 'Loaded'
                    });
                  }
                };
              } else {
                //Others
                script.onload = function () {
                  _this3.scripts[name].loaded = true;
                  resolve({
                    script: name,
                    loaded: true,
                    status: 'Loaded'
                  });
                };
              }

              script.onerror = function (error) {
                return resolve({
                  script: name,
                  loaded: false,
                  status: 'Loaded'
                });
              };

              document.getElementsByTagName('head')[0].appendChild(script);
            } else {
              resolve({
                script: name,
                loaded: true,
                status: 'Already Loaded'
              });
            }
          });
        }
      }]);

      return DynamicScriptLoaderService;
    }();

    DynamicScriptLoaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], DynamicScriptLoaderService);
    /***/
  },

  /***/
  "./src/app/templates/faq/faq-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/templates/faq/faq-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: FaqRoutingModule */

  /***/
  function srcAppTemplatesFaqFaqRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqRoutingModule", function () {
      return FaqRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _faq_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./faq.component */
    "./src/app/templates/faq/faq.component.ts");

    var routes = [{
      path: '',
      component: _faq_component__WEBPACK_IMPORTED_MODULE_3__["FaqComponent"]
    }];

    var FaqRoutingModule = function FaqRoutingModule() {
      _classCallCheck(this, FaqRoutingModule);
    };

    FaqRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], FaqRoutingModule);
    /***/
  },

  /***/
  "./src/app/templates/faq/faq.component.css":
  /*!*************************************************!*\
    !*** ./src/app/templates/faq/faq.component.css ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppTemplatesFaqFaqComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlcy9mYXEvZmFxLmNvbXBvbmVudC5jc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/templates/faq/faq.component.ts":
  /*!************************************************!*\
    !*** ./src/app/templates/faq/faq.component.ts ***!
    \************************************************/

  /*! exports provided: FaqComponent */

  /***/
  function srcAppTemplatesFaqFaqComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqComponent", function () {
      return FaqComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_queries_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/queries.service */
    "./src/app/services/queries.service.ts");
    /* harmony import */


    var _assets_json_faq_faq_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../assets/json/faq/faq.json */
    "./src/assets/json/faq/faq.json");

    var _assets_json_faq_faq_json__WEBPACK_IMPORTED_MODULE_4___namespace =
    /*#__PURE__*/
    __webpack_require__.t(
    /*! ../../../assets/json/faq/faq.json */
    "./src/assets/json/faq/faq.json", 1);
    /* harmony import */


    var _assets_json_faq_preguntas_frecuentes_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../assets/json/faq/preguntas-frecuentes.json */
    "./src/assets/json/faq/preguntas-frecuentes.json");

    var _assets_json_faq_preguntas_frecuentes_json__WEBPACK_IMPORTED_MODULE_5___namespace =
    /*#__PURE__*/
    __webpack_require__.t(
    /*! ../../../assets/json/faq/preguntas-frecuentes.json */
    "./src/assets/json/faq/preguntas-frecuentes.json", 1);
    /* harmony import */


    var _services_dynamic_script_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../services/dynamic-script-loader.service */
    "./src/app/services/dynamic-script-loader.service.ts");

    var FaqComponent =
    /*#__PURE__*/
    function () {
      function FaqComponent(activatedRoute, router, queriesService, dynamicScriptLoader) {
        _classCallCheck(this, FaqComponent);

        this.activatedRoute = activatedRoute;
        this.router = router;
        this.queriesService = queriesService;
        this.dynamicScriptLoader = dynamicScriptLoader;
        this.data = [];
        this.json = _assets_json_faq_faq_json__WEBPACK_IMPORTED_MODULE_4__;
      }

      _createClass(FaqComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this4 = this;

          // refresca página
          this.queriesService.getRefresh();
          document.getElementById('arriba').scrollIntoView({
            behavior: 'smooth'
          });
          this.data['param'] = this.activatedRoute.snapshot.paramMap.get('search-term');

          if (this.data['param'] == null) {
            this.data['faqs'] = _assets_json_faq_preguntas_frecuentes_json__WEBPACK_IMPORTED_MODULE_5__['preguntas-frecuente']; //Carga dinámica del script del Kit UC por conflictos en tiempo de ejecución con Angular

            this.dynamicScriptLoader.load('uc-kit');
          } else {
            this.data['faqs'] = _assets_json_faq_preguntas_frecuentes_json__WEBPACK_IMPORTED_MODULE_5__['preguntas-frecuente']; //Carga dinámica del script del Kit UC por conflictos en tiempo de ejecución con Angular

            this.dynamicScriptLoader.load('uc-kit');
            this.queriesService.queryGet('http://localhost:3000/faqsearch').then(function (data) {
              _this4.data['faqs'] = data; //console.log(data);
              //console.log("2");
              //Carga dinámica del script del Kit UC por conflictos en tiempo de ejecución con Angular

              _this4.dynamicScriptLoader.load('uc-kit');
            }, function (error) {
              console.log(error);
            });
          }
        }
      }, {
        key: "filterFaqs",
        value: function filterFaqs(form, $event) {
          $event.preventDefault();

          if (form.form.value) {
            window.location.href = '/faq/search'; // this.router.navigateByUrl('/faq/search');
          }
        }
      }]);

      return FaqComponent;
    }();

    FaqComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_queries_service__WEBPACK_IMPORTED_MODULE_3__["QueriesService"]
      }, {
        type: _services_dynamic_script_loader_service__WEBPACK_IMPORTED_MODULE_6__["DynamicScriptLoaderService"]
      }];
    };

    FaqComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-faq',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./faq.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/faq/faq.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./faq.component.css */
      "./src/app/templates/faq/faq.component.css")).default]
    })], FaqComponent);
    /***/
  },

  /***/
  "./src/app/templates/faq/faq.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/templates/faq/faq.module.ts ***!
    \*********************************************/

  /*! exports provided: FaqModule */

  /***/
  function srcAppTemplatesFaqFaqModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqModule", function () {
      return FaqModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _faq_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./faq-routing.module */
    "./src/app/templates/faq/faq-routing.module.ts");
    /* harmony import */


    var _faq_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./faq.component */
    "./src/app/templates/faq/faq.component.ts");

    var FaqModule = function FaqModule() {
      _classCallCheck(this, FaqModule);
    };

    FaqModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_faq_component__WEBPACK_IMPORTED_MODULE_4__["FaqComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _faq_routing_module__WEBPACK_IMPORTED_MODULE_3__["FaqRoutingModule"]]
    })], FaqModule);
    /***/
  },

  /***/
  "./src/assets/json/faq/faq.json":
  /*!**************************************!*\
    !*** ./src/assets/json/faq/faq.json ***!
    \**************************************/

  /*! exports provided: horizon_header, horizon_filters, horizon_faqs, default */

  /***/
  function srcAssetsJsonFaqFaqJson(module) {
    module.exports = JSON.parse("{\"horizon_header\":{\"active\":true,\"title\":\"Preguntas frecuentes\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Qué es el Repositorio\",\"title\":\"Te encuentras en página de Preguntas frecuentes de la sección Qué es el repositorio\",\"url\":\"/\"},{\"text\":\"Preguntas frecuentes\",\"title\":\"Te encuentras en página de Preguntas frecuentes\",\"url\":\"/faq\"}]}},\"horizon_filters\":{\"active\":true,\"title\":\"\",\"excerpt\":\"\",\"section_search\":{\"active\":true,\"label\":\"\",\"placeholder\":\"Buscar por\",\"icon\":\"search\",\"name\":\"search_filter\"},\"section_filters\":{\"active\":true,\"title\":\"Filtrar por\",\"excerpt\":\"\"},\"section_button\":{\"active\":true,\"icon\":\"tune\",\"text\":\"Filtrar\",\"title\":\"Filtro\"}},\"horizon_faqs\":{\"active\":true,\"title\":\"\",\"excerpt\":\"\"}}");
    /***/
  }
}]);
//# sourceMappingURL=templates-faq-faq-module-es5.js.map