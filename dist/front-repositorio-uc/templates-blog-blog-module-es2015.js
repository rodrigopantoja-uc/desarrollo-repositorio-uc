(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["templates-blog-blog-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/blog/blog.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/templates/blog/blog.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"uc-section uc-section__header mb-60\" >\n    <div class=\"container\">\n        <div class=\"row\">\n        <div class=\"offset-md-0 col-md-9 offset-0 mb-60\" *ngIf=\"data['content']\">\n            <ul class=\"uc-breadcrumb mb-24\" *ngIf=\"data['content']['horizon_header']['section_breadcrumbs']['active']\">\n                <li *ngFor=\"let breadcrumb of data['content']['horizon_header']['section_breadcrumbs']['breadcrumbs']; last as l\" class=\"uc-breadcrumb_item{{(l) ? ' current' : '' }}\">\n                  <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"!breadcrumb['dynamic']\">{{ breadcrumb.text }}</a>\n                  <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"breadcrumb['dynamic']\">{{ particular }}</a>\n                  <i class=\"uc-icon\" *ngIf=\"!l\">keyboard_arrow_right</i>\n                </li>\n            </ul>\n          <a title=\"{{data['content']['horizon_header']['tag']['title']}}\" href=\"{{data['content']['horizon_header']['tag']['url']}}\" class=\"uc-tag mb-8\">{{ data['content']['horizon_header']['tag']['text'] }}</a>\n          <h1 class=\"mb-32\">{{ data['content']['horizon_header']['title'] }}</h1>\n          <p class=\"text-size--20 text-weight--700\">{{ data['content']['horizon_header']['excerpt'] }}</p>\n          <div class=\"uc-text-divider divider-secondary my-16\"></div>\n        </div>\n        </div>\n          <div class=\"row\">\n            <ng-container *ngIf=\"data['content']\">\n              <ng-container *ngFor=\"let content of data['content']['horizon_contents']; let i = index\">\n  \n                <ng-container *ngIf=\"content['type'] == 'text'\">\n                  <div class=\"offset-md-1 col-md-7 offset-0\" *ngIf=\"content['active']\">\n                    <h2 class=\"text-size--24 mb-8\" *ngIf=\"content['title'] != ''\">{{ content['title'] }}</h2>\n                    <p [innerHTML]=\"content['content']\"></p>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'image'\">\n                  <div class=\"col-md-9 my-60\" *ngIf=\"content['active']\" style=\"text-align: center;\">\n                    <figure><img src=\"{{ content['content'] }}\" alt=\"{{ content['alt'] }}\" title=\"{{ content['title'] }}\" class=\"img-fluid\"></figure>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'quote'\">\n                  <blockquote class=\"offset-md-1 col-md-8 offset-0 my-60\" *ngIf=\"content['active']\">\n                    <p class=\"uc-quote quote-lg pl-32\">\"<span [innerHTML]=\"content['content']\"></span>\"</p>\n                  </blockquote>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'quote_bordered'\">\n                  <blockquote class=\"offset-md-1 col-md-8 offset-0 my-60\" *ngIf=\"content['active']\">\n                    <p class=\"uc-quote quote-lg pl-32 quote-bordered\">\"<span [innerHTML]=\"content['content']\"></span>\"</p>\n                  </blockquote>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'list_normal'\">\n                  <div class=\"offset-md-1 col-md-7 offset-0\" *ngIf=\"content['active']\">\n                    <ul class=\"uc-list uc-list--bins mb-0\">\n                      <li class=\"uc-list__item mb-16\" *ngFor=\"let item of content['content']\" [innerHTML]=\"item['text']\"></li>\n                    </ul>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'Noticias_Destacadas'\">\n                  <div class=\"offset-md-1 col-md-8 offset-0 mt-60\" *ngIf=\"content['active']\">\n                    <h2 class=\"text-size--24 mb-32\">{{ content['title'] }}</h2>\n                    <div class=\"row mb-60\">\n                      <ng-container *ngFor=\"let slide of data['news']; let i = index\">\n                        <article class=\"col-md-4 col-12 mb-2\" *ngIf=\"slide.status == 'published'  && slide.id == '0300bda3e1f0'\">\n                          <a [routerLink]=\"['/blog/',slide.id]\" class=\"uc-card card-height--same mouse-over\"\n                            title=\"{{slide.name}}\">\n                            <figure class=\"uc-card_img\">\n                              <img src=\"{{slide.title}}\" onerror=\"this.src= 'assets/img/default-news.jpg'\" class=\"img-fluid\"\n                                alt=\"{{slide.title}}\">\n                            </figure>\n          \n                            <div class=\"uc-card_body\">\n                              <h3 class=\"text-size--20 text-weight--700\">\n                                {{slide.name}}\n                              </h3>\n                              <div class=\"uc-text-divider divider-primary mt-16\" *ngIf=\"divider\"></div>\n                              <div class=\"text-right mt-auto pt-3\">\n                                <a class=\"uc-link no-decoration link\">\n                                  Seguir leyendo\n                                </a>\n                              </div>\n                            </div>\n                          </a>\n                        </article>\n                      </ng-container>\n                    </div>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'list_links'\">\n                  <div class=\"offset-md-1 col-md-8 offset-0 mt-16\" *ngIf=\"content['active']\">\n                    <h2 class=\"text-size--24 mb-8\">{{ content['title'] }}</h2>\n                    <ul class=\"uc-list uc-list--bins mb-0\">\n                      <li class=\"uc-list__item mb-8\" *ngFor=\"let item of content['content']\">\n                        <a title=\"{{ item['title'] }}\" href=\"{{ item['url'] }}\" class=\"uc-link no-decoration text-weight--500\">\n                          {{ item['text'] }}<i class=\"uc-icon text-size--12\">{{ item['icon'] }}</i>\n                        </a>\n                      </li>\n                    </ul>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'table'\">\n                  <div class=\"col-md-9 my-60\" *ngIf=\"content['active']\">\n                    <table class=\"uc-table\">\n                      <thead>\n                        <tr>\n                          <th *ngFor=\"let th of content['content']['columns']\">{{ th }}</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let td of content['content']['rows']\">\n                          <td><strong>{{ td['hora'] }}</strong></td>\n                          <td>{{ td['seccion'] }}</td>\n                        </tr>\n                      </tbody>\n                    </table>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'video'\">\n                  <div class=\"col-md-9 my-60\" *ngIf=\"content['active']\">\n                    <article class=\"box box--video\">\n                      <video data-role=\"video-player\" src=\"{{ content['content'] }}\" type=\"video/mp4\" controls></video>\n                    </article>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'map'\">\n                  <div class=\"col-md-9\" *ngIf=\"content['active']\">\n                    <h2>{{ content['title'] }}</h2>\n                    <span class=\"uc-subtitle\">{{ content['content']['direccion'] }}</span>\n                    <div id=\"map_{{ i }}\" class=\"map-leaflet\"></div>\n                  </div>\n                </ng-container>\n  \n                <ng-container *ngIf=\"content['type'] == 'descargados'\">\n  \n                  \n                  <div class=\"offset-md-1 col-md-10 offset-0\">\n                    <div class=\"row\">\n                      <div class=\"col\">\n                        \n                        <div class=\"text-align--right\">\n                          <a target=\"blank\" href=\"https://datastudio.google.com/embed/reporting/2b5e2261-3f4c-4915-99a4-afad254db498/page/vgHcB\" class=\"uc-btn btn-inline text-right\">Si no visualiza, ver desde acá<i class=\"uc-icon icon-shape--rounded\">launch</i></a>\n                        </div>                    \n  \n                        <iframe src=\"https://datastudio.google.com/embed/reporting/2b5e2261-3f4c-4915-99a4-afad254db498/page/vgHcB\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\n                      </div>\n                    </div>\n                  </div>\n                </ng-container>\n  \n              </ng-container>              \n            </ng-container>\n  \n          </div>\n    </div>\n  </section>\n  ");

/***/ }),

/***/ "./src/app/templates/blog/blog-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/templates/blog/blog-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: BlogRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogRoutingModule", function() { return BlogRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _blog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blog.component */ "./src/app/templates/blog/blog.component.ts");




const routes = [
    { path: '', component: _blog_component__WEBPACK_IMPORTED_MODULE_3__["BlogComponent"] },
];
let BlogRoutingModule = class BlogRoutingModule {
};
BlogRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], BlogRoutingModule);



/***/ }),

/***/ "./src/app/templates/blog/blog.component.css":
/*!***************************************************!*\
  !*** ./src/app/templates/blog/blog.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlcy9ibG9nL2Jsb2cuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/templates/blog/blog.component.ts":
/*!**************************************************!*\
  !*** ./src/app/templates/blog/blog.component.ts ***!
  \**************************************************/
/*! exports provided: BlogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogComponent", function() { return BlogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_queries_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/queries.service */ "./src/app/services/queries.service.ts");
/* harmony import */ var _assets_json_noticias_noticias_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../assets/json/noticias/noticias.json */ "./src/assets/json/noticias/noticias.json");
var _assets_json_noticias_noticias_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../assets/json/noticias/noticias.json */ "./src/assets/json/noticias/noticias.json", 1);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let BlogComponent = class BlogComponent {
    constructor(activatedRoute, queriesService) {
        this.activatedRoute = activatedRoute;
        this.queriesService = queriesService;
        this.data = [];
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        // Recibo datos por get
        this.activatedRoute.params.subscribe(params => {
            this.id_post = params;
            this.data['content'] = _assets_json_noticias_noticias_json__WEBPACK_IMPORTED_MODULE_3__['section_childs']['childs'][params['id']];
        });
    }
};
BlogComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _services_queries_service__WEBPACK_IMPORTED_MODULE_2__["QueriesService"] }
];
BlogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-blog',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./blog.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/blog/blog.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./blog.component.css */ "./src/app/templates/blog/blog.component.css")).default]
    })
], BlogComponent);



/***/ }),

/***/ "./src/app/templates/blog/blog.module.ts":
/*!***********************************************!*\
  !*** ./src/app/templates/blog/blog.module.ts ***!
  \***********************************************/
/*! exports provided: BlogModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogModule", function() { return BlogModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _blog_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blog-routing.module */ "./src/app/templates/blog/blog-routing.module.ts");
/* harmony import */ var _blog_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./blog.component */ "./src/app/templates/blog/blog.component.ts");





let BlogModule = class BlogModule {
};
BlogModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _blog_component__WEBPACK_IMPORTED_MODULE_4__["BlogComponent"],
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _blog_routing_module__WEBPACK_IMPORTED_MODULE_3__["BlogRoutingModule"],
        ]
    })
], BlogModule);



/***/ })

}]);
//# sourceMappingURL=templates-blog-blog-module-es2015.js.map