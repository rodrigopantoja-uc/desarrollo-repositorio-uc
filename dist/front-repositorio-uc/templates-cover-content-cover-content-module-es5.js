function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["templates-cover-content-cover-content-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/cover-content/cover-content.component.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/templates/cover-content/cover-content.component.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppTemplatesCoverContentCoverContentComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"uc-section uc-section__header mb-60\" *ngIf=\"main['horizon_header']['active']\">\n  <div class=\"container\">\n    <!-- <app-breadcrumbs [breadcrumbs]=\"main['horizon_header']['section_breadcrumbs']['breadcrumbs']\" *ngIf=\"main['horizon_header']['section_breadcrumbs']['active']\"></app-breadcrumbs> -->\n    \n    <ul class=\"uc-breadcrumb mb-24\" *ngIf=\"main['horizon_header']['section_breadcrumbs']['active']\">\n      <li *ngFor=\"let breadcrumb of main['horizon_header']['section_breadcrumbs']['breadcrumbs']; last as l\" class=\"uc-breadcrumb_item{{(l) ? ' current' : '' }}\">\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"!breadcrumb['dynamic']\">{{ breadcrumb.text }}</a>\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"breadcrumb['dynamic']\">{{ particular }}</a>\n        <i class=\"uc-icon\" *ngIf=\"!l\">keyboard_arrow_right</i>\n      </li>\n    </ul>\n\n    <div class=\"row\">\n      <div class=\"col-md-8 col-12\">\n        <h1>{{ main['horizon_header']['title'] }}</h1>\n        <div class=\"uc-excerpt\" *ngIf=\"main['horizon_header']['section_excerpt']['active']\" [innerHTML]=\"main['horizon_header']['section_excerpt']['excerpt']\">\n        </div>\n\n        <div class=\"row mt-100\" *ngIf=\"main['horizon_header']['section_banner']['active']\">\n          <ng-container *ngFor=\"let page of main['horizon_header']['section_banner']['banner']\">\n            <div class=\"col-12 mb-32\">\n              <article class=\"uc-card card-type--horizontal card-height--same\">\n                <figure class=\"uc-card_img\" [style.background-image]=\"'url(' + page.image + ')'\">\n                  <div class=\"uc-veil uc-veil--blue\"></div>\n                </figure>\n                <div class=\"uc-card_body\">\n                  <ng-container *ngIf=\"page['tag']\">\n                    <a title=\"{{ page['tag']['title'] }}\" target=\"page['link']['target'] }}\" href=\"{{ page['tag']['url'] }}\" class=\"uc-tag mb-12\">{{ page['tag']['text'] }}</a>\n                  </ng-container>\n                  <h3 class=\"uc-card__title mb-16\">\n                    <a title=\"{{ page['link']['title'] }}\" target=\"page['link']['target'] }}\" href=\"{{ page['link']['url'] }}\">{{ page['title'] }}</a>\n                  </h3>\n                  <div class=\"uc-text-divider divider-primary\"></div>\n                  <div class=\"uc-excerpt mb-24\" ><p>{{ page['excerpt'] }}</p></div>\n                  <a title=\"{{ page['link']['title'] }}\" target=\"page['link']['target'] }}\"  href=\"{{ page['link']['url'] }}\" class=\"uc-link no-decoration text-align--right mt-auto\">\n                    <strong>{{ page['link']['text'] }}<i class=\"uc-icon m-2\">{{ page['link']['icon'] }}</i></strong>\n                  </a>\n                </div>\n              </article>\n            </div>\n          </ng-container>\n        </div>\n      </div>\n      <div class=\"col-md-4 col-12\" *ngIf=\"main['horizon_header']['section_sidebar']['active']\" >\n        <ng-container *ngFor=\"let sidebar of main['horizon_header']['section_sidebar']['sidebar']\">\n          <button tabindex=0 [routerLink]=\"[ sidebar['link'] ]\" (keypress)=\"[ sidebar['link'] ]\" type=\"button\" class=\"uc-btn btn-cta mt-16\">{{ sidebar['boton'] }}</button>\n        </ng-container>\n        \n      </div>\n    </div>\n  </div>\n</section>\n\n<section class=\"uc-section uc-section__page\" *ngIf=\"main['section_childs']['active']\">\n  <div class=\"container\">\n    <div class=\"row\" >\n      <ng-container *ngFor=\"let page of main['section_childs']['childs']\">\n      <div class=\"col-lg-6 mb-32\" >\n        <!-- <app-card-horizontal [data]=\"page\" [dataOrientation]=\"'image-left'\" [dataDivider]=\"true\"></app-card-horizontal> -->\n        <article class=\"uc-card card-type--horizontal card-height--same\">\n          <figure class=\"uc-card_img\" [style.background-image]=\"'url(' + page.image + ')'\">\n\n            <div class=\"uc-veil uc-veil--blue\"></div>\n          </figure>\n          <div class=\"uc-card_body\">\n            <ng-container *ngIf=\"page['tag']\">\n              <a title=\"{{ page['tag']['title'] }}\" target=\"_blank\"  href=\"{{ page['tag']['url'] }}\" class=\"uc-tag mb-12\">{{ page['tag']['text'] }}</a>\n            </ng-container>\n            <h3 class=\"uc-card__title mb-16\">\n              <a title=\"{{ page['link']['title'] }}\" target=\"{{page['link']['target'] }}\" href=\"{{ page['link']['url'] }}\">{{ page['title'] }}</a>\n            </h3>\n            <div class=\"uc-text-divider divider-primary\"></div>\n            <div class=\"uc-excerpt mb-24\" ><p>{{ page['excerpt'] }}</p></div>\n            <a title=\"{{ page['link']['title'] }}\" target=\"{{page['link']['target'] }}\" href=\"{{ page['link']['url'] }}\" class=\"uc-link no-decoration text-align--right mt-auto\">\n              <strong>{{ page['link']['text'] }}<i class=\"uc-icon\">{{ page['link']['icon'] }}</i></strong> \n            </a>\n          </div>\n        </article>\n\n      </div>\n    </ng-container>\n    </div>\n  </div>\n</section>\n\n<!-- ++++++++++ HELP ++++++++++ -->\n\n<section class=\"uc-section uc-section__page\" *ngIf=\"main['section_help'].active\">\n  <div class=\"container mt-100 margin-bottom-100 p-5 border-top\">\n    <div class=\"row\" >\n      <h4 class=\"col-12 font-centered mb-32\">¿Necesitas ayuda?</h4>\n      <ng-container *ngFor=\"let page of main['section_help']['help']\">\n        <div class=\"col-lg-4 mb-50\" >\n          <article class=\"row font-centered\">\n            <!-- <figure class=\"col-12\" style=\"width: 74px; height: 70px;\" [style.background-image]=\"'url(' + page.image + ')'\">\n            </figure> -->\n            <div class=\"col-12 mb-20\">\n              <a title=\"{{ page['link']['title'] }}\" target=\"_blank\"  href=\"{{ page['link']['url'] }}\" class=\"\">\n                <i class=\"uc-icon font-size-20 circulo-lineal\">{{ page['image']}}</i>\n              </a>\n            </div>\n            <div class=\"col-12\">\n              <p>\n                <a title=\"{{ page['link']['title'] }}\" target=\"_blank\" href=\"{{ page['link']['url'] }}\">{{ page['title'] }}</a>\n                <a title=\"{{ page['link']['title'] }}\" target=\"_blank\"  href=\"{{ page['link']['url'] }}\" class=\"uc-link no-decoration text-align--right mt-auto ml-2\">\n                  <strong>{{ page['link']['text'] }}<i class=\"uc-icon\">{{ page['link']['icon'] }}</i></strong>\n                </a>\n              </p>\n            </div>\n          </article>\n\n        </div>\n      </ng-container>\n      <!-- <P class=\"col-12 font-centered mt-16\">Si necesita apoyo, contáctenos a: <a href=\"mailto:gestiondedatos@uc.cl\">gestiondedatos@uc.cl</a></P> -->\n    </div>\n  </div>\n</section>\n";
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: CoverContentRoutingModule */

  /***/
  function srcAppTemplatesCoverContentCoverContentRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CoverContentRoutingModule", function () {
      return CoverContentRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _cover_content_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./cover-content.component */
    "./src/app/templates/cover-content/cover-content.component.ts");

    var routes = [{
      path: '',
      component: _cover_content_component__WEBPACK_IMPORTED_MODULE_3__["CoverContentComponent"]
    }];

    var CoverContentRoutingModule = function CoverContentRoutingModule() {
      _classCallCheck(this, CoverContentRoutingModule);
    };

    CoverContentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CoverContentRoutingModule);
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content.component.css":
  /*!*********************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content.component.css ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppTemplatesCoverContentCoverContentComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlcy9jb3Zlci1jb250ZW50L2NvdmVyLWNvbnRlbnQuY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content.component.ts":
  /*!********************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content.component.ts ***!
    \********************************************************************/

  /*! exports provided: CoverContentComponent */

  /***/
  function srcAppTemplatesCoverContentCoverContentComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CoverContentComponent", function () {
      return CoverContentComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../assets/json/cover-content/enlaces.json */
    "./src/assets/json/cover-content/enlaces.json");

    var _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3___namespace =
    /*#__PURE__*/
    __webpack_require__.t(
    /*! ../../../assets/json/cover-content/enlaces.json */
    "./src/assets/json/cover-content/enlaces.json", 1);
    /* harmony import */


    var _assets_json_cajon_busqueda_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../assets/json/cajon-busqueda.json */
    "./src/assets/json/cajon-busqueda.json");

    var _assets_json_cajon_busqueda_json__WEBPACK_IMPORTED_MODULE_4___namespace =
    /*#__PURE__*/
    __webpack_require__.t(
    /*! ../../../assets/json/cajon-busqueda.json */
    "./src/assets/json/cajon-busqueda.json", 1);
    /* harmony import */


    var _services_queries_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/queries.service */
    "./src/app/services/queries.service.ts");

    var CoverContentComponent =
    /*#__PURE__*/
    function () {
      function CoverContentComponent(activatedRoute, router, queriesService) {
        _classCallCheck(this, CoverContentComponent);

        this.activatedRoute = activatedRoute;
        this.router = router;
        this.queriesService = queriesService;
        this.data = [];
        this.main = _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3__;
        this.CajonBusqueda = _assets_json_cajon_busqueda_json__WEBPACK_IMPORTED_MODULE_4__;
      }

      _createClass(CoverContentComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          // refresca página
          this.queriesService.getRefresh();
          this.activatedRoute.params.subscribe(function (params) {
            _this.pagina = params['pag']; // JQuery ir arriba

            $('body, html').animate({
              scrollTop: '0px'
            }, 300);
          });
        }
      }, {
        key: "ngDoCheck",
        value: function ngDoCheck() {
          this.main = _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3__;
          this.paginas();
        }
      }, {
        key: "paginas",
        value: function paginas() {
          // MENU ENLACES Y RECURSOS
          if (this.pagina == "otros-repositorios") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "recursos-uc") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "datos-investigacion") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "patentes-uc") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "acerca-de-datos-investigacion") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "beneficios-recomendaciones") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "noticias") {
            this.main = this.main[this.pagina];
          }
        }
      }, {
        key: "getSearch",
        value: function getSearch(form, $event) {
          localStorage.setItem('search_form', form.form.value.ucsearch);
          var array_Filtros = [{
            search_by: 'tipo',
            contains: 'contiene',
            term: this.pagina
          }];
          localStorage.setItem('json_filtros', JSON.stringify(array_Filtros));
          localStorage.setItem('searchAdvanced', 'true');
          localStorage.setItem('page', '1');
          this.router.navigate(['/busqueda']);
        }
      }]);

      return CoverContentComponent;
    }();

    CoverContentComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_queries_service__WEBPACK_IMPORTED_MODULE_5__["QueriesService"]
      }];
    };

    CoverContentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cover-content',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./cover-content.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/cover-content/cover-content.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./cover-content.component.css */
      "./src/app/templates/cover-content/cover-content.component.css")).default]
    })], CoverContentComponent);
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content.module.ts ***!
    \*****************************************************************/

  /*! exports provided: CoverContentModule */

  /***/
  function srcAppTemplatesCoverContentCoverContentModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CoverContentModule", function () {
      return CoverContentModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _cover_content_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./cover-content-routing.module */
    "./src/app/templates/cover-content/cover-content-routing.module.ts");
    /* harmony import */


    var _cover_content_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./cover-content.component */
    "./src/app/templates/cover-content/cover-content.component.ts");

    var CoverContentModule = function CoverContentModule() {
      _classCallCheck(this, CoverContentModule);
    };

    CoverContentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_cover_content_component__WEBPACK_IMPORTED_MODULE_4__["CoverContentComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _cover_content_routing_module__WEBPACK_IMPORTED_MODULE_3__["CoverContentRoutingModule"]]
    })], CoverContentModule);
    /***/
  },

  /***/
  "./src/assets/json/cover-content/enlaces.json":
  /*!****************************************************!*\
    !*** ./src/assets/json/cover-content/enlaces.json ***!
    \****************************************************/

  /*! exports provided: otros-repositorios, recursos-uc, patentes-uc, acerca-de-datos-investigacion, beneficios-recomendaciones, noticias, default */

  /***/
  function srcAssetsJsonCoverContentEnlacesJson(module) {
    module.exports = JSON.parse("{\"otros-repositorios\":{\"horizon_header\":{\"active\":true,\"title\":\"Otros Repositorios\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Otros Repositorios\",\"title\":\"Te encuentras en la página Otros Repositorios de la sección Enlaces y Recursos\",\"url\":\"/enlaces/otros-repositorios\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Creemos que la colaboración e intercambio de información entre las distintas instituciones que estén generando investigación es fundamental para el nacimiento de nuevas ideas y soluciones. Compartimos a continuación una selección de directorios de repositorios nacionales e internacionales que disponen de valiosa información en acceso abierto.\"},\"section_banner\":{\"active\":false,\"banner\":[{\"excerpt\":\"Apoyo de Bibliotecas UC para la Gestión de datos de investigación\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Visitar web para más detalles\",\"title\":\"link a Beneficios para investigadores\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Beneficios para investigadores\"}]},\"section_sidebar\":{\"active\":false,\"sidebar\":[{\"boton\":\"\",\"link\":\"\"}]}},\"section_childs\":{\"active\":true,\"childs\":[{\"excerpt\":\"\",\"image\":\"assets/img/apollo_logo.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Apollo University of Cambridge\",\"url\":\"https://www.repository.cam.ac.uk/\"},\"title\":\"Apollo - University of Cambridge\"},{\"excerpt\":\"\",\"image\":\"assets/img/respositorio-academico-uchile.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Repositorio Académico Universidad de Chile\",\"url\":\"http://repositorio.uchile.cl/\"},\"title\":\"Repositorio Académico - Universidad de Chile\"},{\"excerpt\":\"\",\"image\":\"assets/img/digital-csic.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Repositorio institucional del Consejo Superior de Investigaciones Científicas España.\",\"url\":\"https://digital.csic.es/\"},\"title\":\"Repositorio institucional del Consejo Superior de Investigaciones Científicas España.\"},{\"excerpt\":\"\",\"image\":\"assets/img/upc.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Repositorio Universidad Politécnica de Cataluña, Barcelona, España\",\"url\":\"https://upcommons.upc.edu/\"},\"title\":\"Repositorio Universidad Politécnica de Cataluña, Barcelona, España\"},{\"excerpt\":\"\",\"image\":\"assets/img/rua_medi.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Repositorio institucional de la Universidad de Alicante, España\",\"url\":\"http://rua.ua.es/dspace/\"},\"title\":\"Repositorio institucional de la Universidad de Alicante, España\"},{\"excerpt\":\"\",\"image\":\"assets/img/universidad-complutense-madrid.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a E-prints Universidad Complutense\",\"url\":\"http://eprints.ucm.es/\"},\"title\":\"E-prints Universidad Complutense\"},{\"excerpt\":\"\",\"image\":\"assets/img/repec.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a RePEc  Research Papers in Economic\",\"url\":\"http://repec.org/\"},\"title\":\"RePEc  Research Papers in Economic\"},{\"excerpt\":\"\",\"image\":\"assets/img/philpapers.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a PhilPapers\",\"url\":\"https://philpapers.org/\"},\"title\":\"PhilPapers\"},{\"excerpt\":\"\",\"image\":\"assets/img/lume.jpeg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Repositorio Digital LUME, Brasil\",\"url\":\"https://www.lume.ufrgs.br/\"},\"title\":\"Repositorio Digital LUME, Brasil\"},{\"excerpt\":\"\",\"image\":\"assets/img/gredos.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a GREDOS Universidad de Salamanca\",\"url\":\"https://gredos.usal.es/\"},\"title\":\"GREDOS Universidad de Salamanca\"},{\"excerpt\":\"\",\"image\":\"assets/img/dspace.gif\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a DSpace MIT\",\"url\":\"https://dspace.mit.edu/\"},\"title\":\"DSpace MIT\"},{\"excerpt\":\"\",\"image\":\"assets/img/EuropePMC1.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Europe PMC\",\"url\":\"https://europepmc.org/\"},\"title\":\"Europe PMC\"},{\"excerpt\":\"\",\"image\":\"assets/img/stacked-logo.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a eScholarship Universidad de California\",\"url\":\"https://escholarship.org/repository\"},\"title\":\"eScholarship Universidad de California\"},{\"excerpt\":\"\",\"image\":\"assets/img/eur_signature.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a Repositorio de Tesis de la Universidad de Erasmus, Países Bajos\",\"url\":\"https://thesis.eur.nl/\"},\"title\":\"Repositorio de Tesis de la Universidad de Erasmus, Países Bajos\"},{\"excerpt\":\"\",\"image\":\"https://www.re3data.org/bundles/kitlibraryre3dataapp/img/re3datalogo_black.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"target\":\"_blank\",\"title\":\"link a re3data\",\"url\":\"https://www.re3data.org/\"},\"title\":\"re3data\"}]},\"section_help\":{\"active\":false,\"help\":[{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"Link a Consulta nuestra guía\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/inicio\"},\"title\":\"Consulta nuestra guía\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Cómo subir datos al Repositorio UC\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Cómo subir datos al Repositorio UC\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Checklist para compartir datos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/checklist\"},\"title\":\"Checklist para compartir datos\"}]}},\"recursos-uc\":{\"horizon_header\":{\"active\":true,\"title\":\"Recursos UC\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Recursos UC\",\"title\":\"Te encuentras en la página Recursos UC de la sección Enlaces y Recursos\",\"url\":\"/enlaces/recursos-uc\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Con el objetivo de potenciar su labor de investigación, preservación y difusión del conocimiento, la  Pontificia Universidad Católica de Chile ha puesto a disposición de toda la comunidad una serie de recursos en acceso abierto en distintas disciplinas.\"},\"section_banner\":{\"active\":false,\"banner\":[{\"excerpt\":\"Apoyo de Bibliotecas UC para la Gestión de datos de investigación\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Visitar web para más detalles\",\"title\":\"link a Beneficios para investigadores\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Beneficios para investigadores\"}]},\"section_sidebar\":{\"active\":false,\"sidebar\":[{\"boton\":\"\",\"link\":\"\"}]}},\"section_childs\":{\"active\":true,\"childs\":[{\"excerpt\":\"\",\"image\":\"http://archivohistorico.uc.cl/images/archivohistorico.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"Archivo Histórico UC\",\"url\":\"http://archivohistorico.uc.cl/\"},\"title\":\"Archivo Histórico UC\"},{\"excerpt\":\"\",\"image\":\"https://archivospatrimoniales.uc.cl/themes/Mirage2/images/slider-patrimonio05.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"Catálogo patrimonial UC\",\"url\":\"https://archivospatrimoniales.uc.cl/\"},\"title\":\"Catálogo patrimonial UC\"},{\"excerpt\":\"\",\"image\":\"assets/img/revistas.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"Revistas UC\",\"url\":\"http://guiastematicas.bibliotecas.uc.cl/editoresUC/editoresuc/listadorevistas\"},\"title\":\"Revistas UC\"}]},\"section_help\":{\"active\":false,\"help\":[{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"Link a Consulta nuestra guía\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/inicio\"},\"title\":\"Consulta nuestra guía\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Cómo subir datos al Repositorio UC\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Cómo subir datos al Repositorio UC\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Checklist para compartir datos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/checklist\"},\"title\":\"Checklist para compartir datos\"}]}},\"patentes-uc\":{\"horizon_header\":{\"active\":true,\"title\":\"Patentes UC\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Repositorio\",\"title\":\"Repositorio\",\"url\":\"/\"},{\"text\":\"Patentes UC\",\"title\":\"Patentes UC\",\"url\":\"/enlaces/patentes-uc\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium.\"}},\"childs\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit etiam urna est facilisis\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit etiam urna est facilisis\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet\"}],\"section_help\":{\"active\":true,\"help\":[{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"Link a Consulta nuestra guía\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/inicio\"},\"title\":\"Consulta nuestra guía\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Cómo subir datos al Repositorio UC\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Cómo subir datos al Repositorio UC\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Checklist para compartir datos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/checklist\"},\"title\":\"Checklist para compartir datos\"}]}},\"acerca-de-datos-investigacion\":{\"horizon_header\":{\"active\":true,\"title\":\"Acerca de Datos de Investigación\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Datos de Investigación\",\"title\":\"Te encuentras en la página de Datos de Investigacion de la sección Acerca de Datos de Investigación\",\"url\":\"/enlaces/acerca-de-datos-investigacion\"},{\"text\":\"Acerca de Datos de Investigación\",\"title\":\"Te encuentras en la página de Datos de Investigacion de la sección Acerca de Datos de Investigación\",\"url\":\"/enlaces/acerca-de-datos-investigacion\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"<p>Los datos de investigación son archivos con información -en distintos formatos- que han sido generados o recopilados como insumo para una investigación.  Estos datos pueden ser de carácter cuantitativo o cualitativo, según las características propias  de cada disciplina y publicación.</p><br><p>Gestionar los datos de su investigación tiene diversos beneficios, entre ellos: cumplir con los requisitos de editores de revistas científicas e instituciones de financiamiento de la investigación (en algunos casos es de carácter obligatorio), asegurar la integridad y reproducibilidad de la investigación y promover la colaboración dentro de la comunidad científica, previniendo la duplicación de esfuerzos al permitir que otros usen los datos.</p><br><p>Si pertenece a la Pontificia Universidad Católica de Chile y le interesa compartir los datos de su investigación, le invitamos a hacerlo en nuestro Repositorio.</p>\"},\"section_banner\":{\"active\":true,\"banner\":[{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Visitar web para más detalles\",\"title\":\"link a consulta nuestra guía\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gestiondedatos\"},\"title\":\"Consulta nuestra guía de ayuda o apoyo\"}]},\"section_sidebar\":{\"active\":true,\"sidebar\":[{\"boton\":\"Buscar Datos de Investigación\",\"link\":\"/busqueda/Datos de Investigación\"},{\"boton\":\"Subir Datos de Investigación\",\"link\":\"/datos-investigacion\"}]}},\"section_childs\":{\"active\":false,\"childs\":[{\"excerpt\":\"Apoyo de Bibliotecas UC para la Gestión de datos de investigación\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Visitar web para más detalles\",\"title\":\"link a Beneficios para investigadores\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Beneficios para investigadores\"}]},\"section_help\":{\"active\":true,\"help\":[{\"excerpt\":\"\",\"image\":\"mail\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"Link a correo\",\"url\":\"mailto:gestiondedatos@uc.cl\"},\"title\":\"Consultas a gestiondedatos@uc.cl\"},{\"excerpt\":\"\",\"image\":\"cloud_upload\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Cómo subir datos al Repositorio UC\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/subirdatos\"},\"title\":\"Cómo subir datos al Repositorio UC\"},{\"excerpt\":\"\",\"image\":\"checklist_rtl\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Checklist para compartir datos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/checklist\"},\"title\":\"Checklist para compartir datos\"}]}},\"beneficios-recomendaciones\":{\"horizon_header\":{\"active\":true,\"title\":\"Beneficios y Recomendaciones\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Datos de Investigación\",\"title\":\"Te encuentras en la página de Datos de Investigacion de la sección Enlaces y Recursos\",\"url\":\"/enlaces/datos-investigacion\"},{\"text\":\"Beneficios y recomendaciones\",\"title\":\"Te encuentras en la página de Datos de Investigacion de la sección Enlaces y Recursos\",\"url\":\"/enlaces/datos-investigacion\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Preservar y compartir los datos que se recopilan y generan al desarrollar una investigación es beneficioso tanto para quienes los publican como para quienes los usan. Esta práctica genera mayor confiabilidad en los resultados de las investigaciones, ahorra tiempo y recursos en la producción de nuevos estudios y, además,  forma parte de los nuevos requisitos de muchos financiadores de investigación y editores de revistas. <br><br>Bibliotecas UC pone a disposición de los investigadores de la Universidad el Repositorio institucional, plataforma donde se podrá buscar, compartir y preservar los datos de investigación. <br><br>Revise a continuación mayor información y, si necesita apoyo, contáctenos a: <a href='mailto:gestiondedatos@uc.cl'>gestiondedatos@uc.cl</a>\"}},\"childs\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Beneficios para investigadores\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Beneficios para investigadores\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/DI-checklist.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Checklist para Compartir Datos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/checklist\"},\"title\":\"Checklist para compartir datos\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/DI-subir.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Cómo subir datos al Repositorio UC\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/subirdatos\"},\"title\":\"Cómo subir datos al Repositorio UC\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/DI-consulte.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Repositorio de Tesis de la Universidad de Erasmus, Países Bajos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/inicio\"},\"title\":\"Consulte nuestra guía\"}],\"section_help\":{\"active\":true,\"help\":[{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"Link a Consulta nuestra guía\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/inicio\"},\"title\":\"Consulta nuestra guía\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Cómo subir datos al Repositorio UC\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Cómo subir datos al Repositorio UC\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Checklist para compartir datos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/checklist\"},\"title\":\"Checklist para compartir datos\"}]}},\"noticias\":{\"horizon_header\":{\"active\":true,\"title\":\"Noticias\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Noticias\",\"title\":\"Te encuentras en la página Noticias\",\"url\":\"/enlaces/noticias\"}]},\"section_excerpt\":{\"active\":false,\"excerpt\":\"Con el objetivo de potenciar su labor de investigación, preservación y difusión del conocimiento, la  Pontificia Universidad Católica de Chile ha puesto a disposición de toda la comunidad una serie de recursos en acceso abierto en distintas disciplinas.\"},\"section_banner\":{\"active\":false,\"banner\":[{\"excerpt\":\"Apoyo de Bibliotecas UC para la Gestión de datos de investigación\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Visitar web para más detalles\",\"title\":\"link a Beneficios para investigadores\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Beneficios para investigadores\"}]},\"section_sidebar\":{\"active\":false,\"sidebar\":[{\"boton\":\"\",\"link\":\"\"}]}},\"section_childs\":{\"active\":true,\"childs\":[{\"excerpt\":\"Puedes acceder y descargar artículos de “autoría UC“ publicados por esta reconocida editorial científica.\",\"image\":\"https://mcusercontent.com/43aecb2da99dc8cbbb106788c/_compresseds/9fe2f1cf-b884-4327-bba1-94838e42fe83.jpg\",\"link\":{\"icon\":\"\",\"text\":\"Seguir leyendo\",\"target\":\"_self\",\"title\":\"Más de 200 títulos de BioMed Central disponibles en acceso abierto\",\"url\":\"/content/biomed\"},\"title\":\"Más de 200 títulos de BioMed Central disponibles en acceso abierto\"},{\"excerpt\":\"Crear un Perfil ORCID y vincularlo con el Repositorio UC fomenta la visibilidad de la producción académica y de investigación.\",\"image\":\"https://mcusercontent.com/43aecb2da99dc8cbbb106788c/images/d5fa5662-0cf2-4b38-83da-d889a0bffbc8.jpg\",\"link\":{\"icon\":\"\",\"text\":\"Seguir leyendo\",\"target\":\"_self\",\"title\":\"ORCID: La plataforma digital que apoya la investigación\",\"url\":\"/content/orcid\"},\"title\":\"ORCID: La plataforma digital que apoya la investigación\"},{\"excerpt\":\"Buscando ofrecer la mejor plataforma de difusión y descubrimiento de la investigación desarrollada en la Universidad Católica, nuestro Repositorio UC ha renovado su interfaz y organización de contenidos.\",\"image\":\"https://mcusercontent.com/43aecb2da99dc8cbbb106788c/images/007a14d5-75db-4de3-837e-aeed0c04f7e9.jpg\",\"link\":{\"icon\":\"\",\"text\":\"Seguir leyendo\",\"target\":\"_self\",\"title\":\"¡Te damos la bienvenida al nuevo Repositorio UC!\",\"url\":\"/content/bienvenida\"},\"title\":\"¡Te damos la bienvenida al nuevo Repositorio UC!\"}]},\"section_help\":{\"active\":false,\"help\":[{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"Link a Consulta nuestra guía\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/inicio\"},\"title\":\"Consulta nuestra guía\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Cómo subir datos al Repositorio UC\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/beneficios\"},\"title\":\"Cómo subir datos al Repositorio UC\"},{\"excerpt\":\"\",\"image\":\"assets/img/DI-beneficios.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"\",\"title\":\"link a Checklist para compartir datos\",\"url\":\"https://guiastematicas.bibliotecas.uc.cl/gdi_ip/checklist\"},\"title\":\"Checklist para compartir datos\"}]}}}");
    /***/
  }
}]);
//# sourceMappingURL=templates-cover-content-cover-content-module-es5.js.map