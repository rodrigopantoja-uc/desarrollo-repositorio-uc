import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { global }     from '../services/global';
import { map }        from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileuploadService {

  public urlPhp;
  URL = global.php+"/autoarchivo/";
  urlfile = global.php + "/autoarchivo/subirArchivo.php"
  

  constructor( private http: HttpClient ) { 
    this.urlPhp = global.php; 
  }

  uploadFile(archivo) {
    return this.http.post(`${this.URL}subirArchivo.php`, JSON.stringify(archivo));
  }

  public subirfile(archivo){
    return new Promise( (resolve, reject) => {
      this.http.post(this.urlfile, JSON.stringify(archivo)).pipe(
        map( (res: Response) => res )
      ).subscribe(
        (data) => {
          resolve(data);
        },
        (err) => {
          console.log(err);
          reject();
        }
      )
    });
  }

  register(archivo): Observable<any>{
    let params = JSON.stringify(archivo);
    console.log(archivo);
    //let params = 'json='+json;

    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(this.urlfile, params, {headers: headers});
}

}
