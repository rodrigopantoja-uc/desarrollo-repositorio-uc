import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescargaGaComponent } from './descarga-ga.component';

describe('DescargaGaComponent', () => {
  let component: DescargaGaComponent;
  let fixture: ComponentFixture<DescargaGaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescargaGaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescargaGaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
