import { Component, OnInit }                      from '@angular/core';
import { QueriesService }                         from '../../services/queries.service';
import json                                       from '../../../assets/json/noticias/noticias.json';
import { ActivatedRoute, Params }                 from '@angular/router';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {
  main: any = json;

  constructor(
    private activatedRoute: ActivatedRoute,
    private queriesService: QueriesService
    ) { 
    }

  ngOnInit() {

    // refresca página
    this.queriesService.getRefresh();

    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300); 

  }
}



