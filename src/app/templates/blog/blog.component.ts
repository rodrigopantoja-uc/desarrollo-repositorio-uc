import { Component, OnInit }                      from '@angular/core';
import { QueriesService }                         from '../../services/queries.service';
import json                                       from '../../../assets/json/noticias/noticias.json';
import { ActivatedRoute, Params }                 from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  data: any = [];
  id_post;

  constructor(
    private activatedRoute: ActivatedRoute,
    private queriesService: QueriesService
    ) { 
    }

    ngOnInit() {
      // refresca página
      this.queriesService.getRefresh();
      
      // JQuery ir arriba
        $('body, html').animate({
          scrollTop: '0px'
        }, 300); 

    // Recibo datos por get
    this.activatedRoute.params.subscribe(params =>{
      this.id_post = params;
      this.data['content'] = json['section_childs']['childs'][params['id']];
    });

  }


}



