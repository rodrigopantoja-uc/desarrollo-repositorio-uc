import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreacionArtisticaRoutingModule } from './creacion-artistica-routing.module';
import { CreacionArtisticaComponent} from './creacion-artistica.component';
/* import { BreadcrumbsComponent } from '../../partials/commons/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from '../../partials/commons/title/title.component'; */

@NgModule({
  declarations: [
    CreacionArtisticaComponent
/*     BreadcrumbsComponent,
    TitleComponent */
  ],
  imports: [
    CommonModule,
    CreacionArtisticaRoutingModule
  ]
})
export class CreacionArtisticaModule { }
