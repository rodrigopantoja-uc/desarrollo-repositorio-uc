import { Component, OnInit, ChangeDetectionStrategy, DoCheck, ɵConsole }  from '@angular/core';
import { animate, state, style, transition, trigger }           from '@angular/animations';
import { Router, ActivatedRoute }                               from '@angular/router';
import * as CryptoJS                                            from 'crypto-js';

import json                                                     from '../../../assets/json/creacion-artistica/creacion-artistica.json';
import compartir                                                from '../../../assets/json/upload-record/02-compartir-investigacion.json';
import { FileuploadService }                                    from '../../servicio/fileupload.service';
import { QueriesService }                                       from '../../services/queries.service';
import { global }                                               from '../../services/global';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-creacion-artistica',
  templateUrl: './creacion-artistica.component.html',
  styleUrls: ['./creacion-artistica.component.css'],

  //changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slide', [
      state('t1', style({ transform: 'translateX(0)' })),
      state('t2', style({ transform: 'translateX(-25%)' })),
      state('t3', style({ transform: 'translateX(-50%)' })),
      state('t4', style({ transform: 'translateX(-75%)' })),
      transition('* => *', animate(300))
    ])
  ]
})
export class CreacionArtisticaComponent implements OnInit, DoCheck {

  public password;
  data: any = [];
  json: any = json;
  compartir: any = compartir;
  isLeftVisible = true;
  activePane: any = 't1';
  archivo: any = {
    clave: this._queriesService.getPass(),
    user: localStorage.getItem("correo"),
    autor: null,
    titulo: null,
    fecha_creacion: "0000-00-00",
    enlace_creacion: null,
    nota: null,
    
    fecha: "0000-00-00",
    nombreArchivo: null,
    fileSource: null,
    base64textString: null,
    requerido: null
  }
  public array_resumen:any = [];
  public msj: any;
  usuario;
  
  key = "5t1dd";
  login;
  errLog;

  public identity;
  public token;

  plainText: string;
  encryptText: string;
  encPassword: string;
  decPassword: string;
  conversionEncryptOutput: string;
  conversionDecryptOutput: string;
  urlfile = global.php + "/autoarchivo/creacionArtistica.php"
  soportadateInput;
  pagina;

  today;
  dd;
  mm;
  yyyy;
  rango_date;

  constructor(
    private uploadService: FileuploadService,
    private _queriesService: QueriesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
    ) 
    {
      this.identity = this._queriesService.getIdentity();
      this.token = this._queriesService.getToken();
      this.password = this._queriesService.getPass();
    }

  ngOnInit() {
    // refresca página
    this._queriesService.getRefresh();

    this.activatedRoute.params.subscribe(params =>{
      this.pagina = params['pag'];
    }); 

    this.date();
    this.today = new Date().toISOString().split('T')[0];

    // Después usar wards en routing
    /* if(!this.token){ 
      window.location.href='assets/php/cas-log/cas-login.php';
    } */
    this.usuario = localStorage.getItem('usuario');

    // JQuery ir arriba
    if(this.pagina == 'form'){
      $('body, html').animate({
        scrollTop: '0px'
      }, 300);
    }

    
    //document.getElementById('arriba').scrollIntoView({behavior: 'smooth'});
  }

  ngDoCheck(){
    this.password = this._queriesService.getPass()
  }

  date(){
    var pruebaFecha = document.createElement("input");
    //Si tiene soporte: debe aceptar el tipo "date"...
    pruebaFecha.setAttribute("type", "date");
    if ( pruebaFecha.type === "date" ){
      this.soportadateInput = true;
    }else{
      this.soportadateInput = false;
    }
  }

  fechaHoy(){
    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("input_fecha_publicacion")[0].setAttribute('min', today);
    console.log(this.archivo.fecha)
  }
  /* fechaHoy_(valor){
    this.today = new Date();
    this.dd = this.today.getDate();
    this.mm = this.today.getMonth() + 1; 
    this.yyyy = this.today.getFullYear();
    if (this.dd < 10) {
      this.dd = '0' + this.dd
    }
    if (this.mm < 10) {
      this.mm = '0' + this.mm
    }
console.log(valor)

    if(valor>=this.today){
      this.rango_date = true;
      this.archivo.fecha = valor;
      
    }else{
      this.rango_date = false;
      this.archivo.fecha = valor;
      
    }
    console.log(this.archivo.fecha)
  } */

  toScroll(){
    document.getElementById('pasos').scrollIntoView({behavior: 'smooth'});
  }
  _handleReaderLoaded(readerEvent) {
    var binaryString = readerEvent.target.result;
    this.archivo.base64textString = btoa(binaryString);
  }

  collapse(item){
    $('#'+item).slideToggle(300);
    console.log(this.archivo);
  }
  
  seleccionarArchivo(event) {

/*     var files = event.target.files;
    var file = files[0];
    this.archivo.nombreArchivo = file.name;

    if(files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    } */

    // nuevo
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.archivo.fileSource = file;
      //console.log(file.name);
      console.log(file);
    }
  }

  resetFile(){
    this.archivo.fileSource = "";
    this.archivo.nombreArchivo = "";
  }
  // Nuevo carga
  submitPublica(form){
  
    this.login = false;
    if(this.archivo.fecha == "0000-00-00"){
      var fecha:any = 0;
    }else{
      var fecha:any = this.archivo.fecha;
    }
    const formData = new FormData();
    
    formData.append('name', this.archivo.autor);
    formData.append('titulo', this.archivo.titulo);
    formData.append('fecha_creacion', this.archivo.fecha_creacion);
    formData.append('enlace_creacion', this.archivo.enlace_creacion);
    formData.append('nota', this.archivo.nota);
    formData.append('fecha', fecha);
    formData.append('file', this.archivo.fileSource);
    formData.append('user', localStorage.getItem("correo"));
    formData.append('clave', this.archivo.clave);

    console.log(this.urlfile);
    console.log(formData);
    console.log(this.archivo);

    this.http.post(this.urlfile, formData)
      .subscribe(response => {

        if(response['resultado'] == "200"){
          this.msj = "success";
          form.reset();
          this.archivo.nombreArchivo = null;
          this.archivo.fecha = "0000-00-00";
          this.archivo.requerido = false;
          this.archivo = [];
          console.log(response);
          
        }else if(response['resultado']== "403"){
          this.msj = false;
          this.login = true;
          this.errLog = true;
          this.archivo.clave = null;
          console.log("se conectó pero login es incorrecto");
          console.log(response['resultado'])
          
        }else{
          this.msj = "error";
          form.reset();
          this.archivo.nombreArchivo= null;
          this.archivo.base64textString= null;
          this.archivo.requerido= false;
          console.log("se conectó pero no trajo resultado 200");
          console.log(response)
        }
      })

  }

  loguearse(){
    this.login = true;
    this.errLog = false;
  }
  closeLogin(){
    this.login = false;
  }

  modal(){

    var modal = document.getElementById("tvesModal");
    var body = document.getElementsByTagName("body")[0];

    modal.style.display = "block";
    body.style.position = "static";
    body.style.height = "100%";
    body.style.overflow = "hidden";

        window.onclick = function(event) {
          if (event.target == modal) {
            modal.style.display = "none";

            body.style.position = "inherit";
            body.style.height = "auto";
            body.style.overflow = "visible";
          }
        }
  }
  closeModal(){
    var modal = document.getElementById("tvesModal");
    var body = document.getElementsByTagName("body")[0];

    modal.style.display = "none";
    body.style.position = "inherit";
    body.style.height = "auto";
    body.style.overflow = "visible";

  }
  newUpload(form){
    this.msj= "";
    this.activePane= 't1';
  }

  tabla_resumen(){
    console.log(this.archivo);
    this.array_resumen = [];
    var alias;
    var alias_valor;
    var mostrar = true;
    for(let i in this.archivo){
      if(i == "autor"){
        alias = "Autor principal";
        mostrar = true;
        alias_valor = this.archivo[i];
      }
      if(i == "titulo"){
        alias = "Título";
        alias_valor = this.archivo[i];
        mostrar = true;
      }
      if(i == "fecha_creacion"){
        alias = "Fecha de creación";
        alias_valor = this.archivo[i];
        mostrar = true;
      }
      if(i == "enlace_creacion"){
        alias = "Enlace de creación";
        alias_valor = this.archivo[i];
        mostrar = true;
      }
      if(i == "nota"){
        alias = "Nota";
        alias_valor = this.archivo[i];
        mostrar = true;
      }
      if(i == "clave"){
        alias = "Clave";
        mostrar = false;
      }
      if(i == "user"){
        alias = "Usuario";
        mostrar = false;
      }
      if(i == "fecha"){
        alias = "Fecha de embargo";
        alias_valor = this.archivo[i];
        mostrar = true;
      }
      if(i == "nombreArchivo"){
        alias = "Nombre del archivo";
        alias_valor = this.archivo[i];
        mostrar = true;
      }
      if(i == "fileSource"){
        alias = "fileSource";
        mostrar = false;
      }
      if(i == "base64textString"){
        alias = "base64textString";
        mostrar = false;
      }
      if(i == "requerido"){
        alias = "Licencia";
        mostrar = true;
        if(this.archivo[i] == true){
          alias_valor = "Concedo licencia";
        }
      }

      if(mostrar == true){
        this.array_resumen.push( { "param":alias, "valor": alias_valor});
      }
    }

  }

}
