import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreacionArtisticaComponent} from './creacion-artistica.component';

const routes: Routes = [
  { path: '',                   component: CreacionArtisticaComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreacionArtisticaRoutingModule { }
