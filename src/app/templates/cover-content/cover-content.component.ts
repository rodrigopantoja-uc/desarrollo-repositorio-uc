import { Component, OnInit, DoCheck }             from '@angular/core';
import { Router, ActivatedRoute, Params }         from '@angular/router';
import json                                       from '../../../assets/json/cover-content/enlaces.json';
import CajonBusqueda                              from '../../../assets/json/cajon-busqueda.json';
import { QueriesService }                         from '../../services/queries.service';

@Component({
  selector: 'app-cover-content',
  templateUrl: './cover-content.component.html',
  styleUrls: ['./cover-content.component.css']
})
export class CoverContentComponent implements OnInit, DoCheck {

  data: any = [];
  main: any = json;
  CajonBusqueda: any = CajonBusqueda;

  public pagina: string;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router,
    private queriesService: QueriesService
  ) { }

  ngOnInit() {

    // refresca página
    this.queriesService.getRefresh();

    this.activatedRoute.params.subscribe(params =>{
      this.pagina = params['pag'];
      // JQuery ir arriba
      $('body, html').animate({
        scrollTop: '0px'
      }, 300);
    });
  }
  ngDoCheck() {
    this.main = json;
    this.paginas();
  }

  paginas(){
      // MENU ENLACES Y RECURSOS
      if(this.pagina == "otros-repositorios"){
        this.main = this.main[this.pagina ];
      }
      if(this.pagina == "recursos-uc"){
        this.main = this.main[this.pagina ];
      }
      if(this.pagina == "datos-investigacion"){
        this.main = this.main[this.pagina ];
      }
      if(this.pagina == "patentes-uc"){
        this.main = this.main[this.pagina ];
      }
      if(this.pagina == "acerca-de-datos-investigacion"){
        this.main = this.main[this.pagina ];
      }
      if(this.pagina == "beneficios-recomendaciones"){
        this.main = this.main[this.pagina ];
      }
      if(this.pagina == "noticias"){
        this.main = this.main[this.pagina ];
      }
  }

  getSearch(form, $event) {
    localStorage.setItem('search_form',form.form.value.ucsearch );
    var array_Filtros: any[] = [
      {
        search_by: 'tipo',
        contains: 'contiene',
        term: this.pagina
      }
    ];
    localStorage.setItem('json_filtros',JSON.stringify(array_Filtros));
    localStorage.setItem('searchAdvanced','true');
    localStorage.setItem('page','1');
    this.router.navigate(['/busqueda']);
  }

}
