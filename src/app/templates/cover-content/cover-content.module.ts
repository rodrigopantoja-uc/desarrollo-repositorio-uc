import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoverContentRoutingModule } from './cover-content-routing.module';
import { CoverContentComponent } from './cover-content.component';

@NgModule({
  declarations: [
    CoverContentComponent
  ],
  imports: [
    CommonModule,
    CoverContentRoutingModule
  ]
})
export class CoverContentModule { }
