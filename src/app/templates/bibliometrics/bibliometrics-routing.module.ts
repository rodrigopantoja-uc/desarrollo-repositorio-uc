import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BibliometricsComponent } from './bibliometrics.component';


const routes: Routes = [
  { path: '',                   component: BibliometricsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BibliometricsRoutingModule { }
