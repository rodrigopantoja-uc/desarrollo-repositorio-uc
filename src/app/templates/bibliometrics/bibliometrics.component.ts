import { Component, OnInit }  from '@angular/core';
import json                   from '../../../assets/json/bibliometrics.json';
import Studies                from '../../../assets/json/studies.json';
import { QueriesService }     from '../../services/queries.service';

@Component({
  selector: 'app-bibliometrics',
  templateUrl: './bibliometrics.component.html',
  styleUrls: ['./bibliometrics.component.css']
})
export class BibliometricsComponent implements OnInit {

  data:               any = [];
  mainBibliometrics:  any = {};

  constructor(
    private queriesService: QueriesService
  ) { }

  ngOnInit() {

    // refresca página
    this.queriesService.getRefresh();

    this.mainBibliometrics = json;
    this.data['studies'] = Studies;

    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);
  }

  scrollTo(element, $event) {
    $event.preventDefault();
    document.getElementById(element).scrollIntoView({behavior: 'smooth'});
  }
}
