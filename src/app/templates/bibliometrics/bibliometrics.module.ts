import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BibliometricsRoutingModule } from './bibliometrics-routing.module';
import { BibliometricsComponent } from './bibliometrics.component';
import { CardBibliometricsComponent } from '../../partials/cards/card-bibliometrics/card-bibliometrics.component';

@NgModule({
  declarations: [
    BibliometricsComponent,
    CardBibliometricsComponent
  ],
  exports: [
    
  ],
  imports: [
    CommonModule,
    BibliometricsRoutingModule
  ]
})
export class BibliometricsModule { }
