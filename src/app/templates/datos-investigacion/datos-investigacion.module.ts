import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatosInvestigacionRoutingModule } from './datos-investigacion-routing.module';
import { DatosInvestigacionComponent} from './datos-investigacion.component';

@NgModule({
  declarations: [
    DatosInvestigacionComponent
  ],
  imports: [
    CommonModule,
    DatosInvestigacionRoutingModule
  ]
})
export class DatosInvestigacionModule { }
