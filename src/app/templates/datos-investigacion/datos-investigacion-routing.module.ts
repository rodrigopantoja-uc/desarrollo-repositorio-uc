import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatosInvestigacionComponent} from './datos-investigacion.component';

const routes: Routes = [
  { path: '',                   component: DatosInvestigacionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatosInvestigacionRoutingModule { }
