import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SingleRecordRoutingModule } from './single-record-routing.module';
import { SingleRecordComponent} from './single-record.component';
import { CardNormalComponent } from '../../partials/cards/card-normal/card-normal.component';
import { TitleComponent } from '../../partials/commons/title/title.component';

@NgModule({
  declarations: [
    SingleRecordComponent,
    CardNormalComponent,
    TitleComponent
  ],
  imports: [
    CommonModule,
    SingleRecordRoutingModule
  ]
})
export class SingleRecordModule { }
