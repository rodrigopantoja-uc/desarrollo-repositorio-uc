import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoverSecondLevelRoutingModule } from './cover-second-level-routing.module';
import { CoverSecondLevelComponent } from './cover-second-level.component';
import { CardNormalComponent } from '../../partials/cards/card-normal/card-normal.component';
import { CardTopicComponent } from '../../partials/cards/card-topic/card-topic.component';
import { CardPublicationsComponent } from '../../partials/cards/card-publications/card-publications.component';
import { BreadcrumbsComponent } from '../../partials/commons/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from '../../partials/commons/title/title.component';
import { SearchFormComponent } from '../../partials/commons/search-form/search-form.component';

@NgModule({
  declarations: [
    CoverSecondLevelComponent,
    CardNormalComponent,
    CardTopicComponent,
    CardPublicationsComponent,
    BreadcrumbsComponent,
    TitleComponent,
    SearchFormComponent
  ],
  imports: [
    CommonModule,
    CoverSecondLevelRoutingModule
  ]
})
export class CoverSecondLevelModule { }
