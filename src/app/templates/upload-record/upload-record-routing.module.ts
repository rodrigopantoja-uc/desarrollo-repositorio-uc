import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadRecordComponent} from './upload-record.component';

const routes: Routes = [
  { path: '',                   component: UploadRecordComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadRecordRoutingModule { }
