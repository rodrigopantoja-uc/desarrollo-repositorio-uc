import { Component, OnInit }  from '@angular/core';
import menu_repositorio_uc    from '../../../assets/json/menu-repositorio.json';
import { Router }             from '@angular/router';
//import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
//import { FormGroup,  FormBuilder,  Validators }   from '@angular/forms';
//import { QueriesService } from '../../services/queries.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  MenuRepos: any = menu_repositorio_uc['menu-repositorio-uc'];
  MenuCorp: any = menu_repositorio_uc['menu-corp'];
  //data:     any = [];
  msj;
  estilo;
  icono;

  constructor(
    private router: Router
    //private queriesService: QueriesService, private _sanitizer: DomSanitizer, private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    $("#msjHome").hide();
    if(localStorage.getItem("correo")){
      this.alertas("success","check_circle","Sesión abierta");
    }
  }

  alertas(estilo, icono, texto){
    $("#msjHome").fadeIn(1000);
    setInterval(() => {
      $("#msjHome").fadeOut(1500);
    }, 2000);
    this.msj = texto;
    this.estilo = estilo;
    this.icono = icono;
  }

  search_header(){
    localStorage.setItem('search_form','');
  }

  /* BusquedaAvanzada() {
    localStorage.setItem('search_form','');
    var array_Filtros: any[] = [
      {
        search_by: 'tipo',
        contains: 'contiene',
        term: 'conjuntos de datos'
      }
    ];
    localStorage.setItem('json_filtros',JSON.stringify(array_Filtros));
    localStorage.setItem('searchAdvanced','true');
    localStorage.setItem('page','1');
    this.router.navigate(['/busqueda']);
  } */
}
