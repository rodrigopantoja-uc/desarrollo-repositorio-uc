import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-normal',
  templateUrl: './card-normal.component.html',
  styleUrls: ['./card-normal.component.css']
})
export class CardNormalComponent implements OnInit {

  @Input() data:    any;
  @Input() divider: any;
  @Input() excerpt: any;

  constructor() { }

  ngOnInit() {
  }

}
